﻿namespace CBridge
{
    partial class CBridge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CBridge));
            this.pbModel = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bBent = new System.Windows.Forms.Button();
            this.bAbutment = new System.Windows.Forms.Button();
            this.bCodeCheck = new System.Windows.Forms.Button();
            this.bLayout = new System.Windows.Forms.Button();
            this.bAlignment = new System.Windows.Forms.Button();
            this.bPS_PT = new System.Windows.Forms.Button();
            this.bLoad = new System.Windows.Forms.Button();
            this.bRebar = new System.Windows.Forms.Button();
            this.bSection = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabInput = new System.Windows.Forms.TabControl();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.helpAboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crossSectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.componentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elementInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openBridgeModel = new System.Windows.Forms.ToolStripMenuItem();
            this.saveModel = new System.Windows.Forms.ToolStripMenuItem();
            this.saveModelAs = new System.Windows.Forms.ToolStripMenuItem();
            this.exitCBridgeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pbModel)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabInput.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbModel
            // 
            this.pbModel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pbModel.Dock = System.Windows.Forms.DockStyle.Right;
            this.pbModel.Location = new System.Drawing.Point(312, 24);
            this.pbModel.Margin = new System.Windows.Forms.Padding(2);
            this.pbModel.Name = "pbModel";
            this.pbModel.Size = new System.Drawing.Size(758, 568);
            this.pbModel.TabIndex = 21;
            this.pbModel.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tabPage1.Controls.Add(this.bBent);
            this.tabPage1.Controls.Add(this.bAbutment);
            this.tabPage1.Controls.Add(this.bCodeCheck);
            this.tabPage1.Controls.Add(this.bLayout);
            this.tabPage1.Controls.Add(this.bAlignment);
            this.tabPage1.Controls.Add(this.bPS_PT);
            this.tabPage1.Controls.Add(this.bLoad);
            this.tabPage1.Controls.Add(this.bRebar);
            this.tabPage1.Controls.Add(this.bSection);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(162, 399);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Bridge Input";
            // 
            // bBent
            // 
            this.bBent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bBent.ForeColor = System.Drawing.Color.DarkRed;
            this.bBent.Location = new System.Drawing.Point(15, 140);
            this.bBent.Margin = new System.Windows.Forms.Padding(2);
            this.bBent.Name = "bBent";
            this.bBent.Size = new System.Drawing.Size(130, 30);
            this.bBent.TabIndex = 3;
            this.bBent.Text = "Bent";
            this.bBent.UseVisualStyleBackColor = true;
            // 
            // bAbutment
            // 
            this.bAbutment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bAbutment.ForeColor = System.Drawing.Color.DarkRed;
            this.bAbutment.Location = new System.Drawing.Point(15, 100);
            this.bAbutment.Margin = new System.Windows.Forms.Padding(2);
            this.bAbutment.Name = "bAbutment";
            this.bAbutment.Size = new System.Drawing.Size(130, 30);
            this.bAbutment.TabIndex = 2;
            this.bAbutment.Text = "Abutment";
            this.bAbutment.UseVisualStyleBackColor = true;
            // 
            // bCodeCheck
            // 
            this.bCodeCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCodeCheck.ForeColor = System.Drawing.Color.DarkRed;
            this.bCodeCheck.Location = new System.Drawing.Point(15, 340);
            this.bCodeCheck.Margin = new System.Windows.Forms.Padding(2);
            this.bCodeCheck.Name = "bCodeCheck";
            this.bCodeCheck.Size = new System.Drawing.Size(130, 30);
            this.bCodeCheck.TabIndex = 8;
            this.bCodeCheck.Text = "CodeCheck";
            this.bCodeCheck.UseVisualStyleBackColor = true;
            // 
            // bLayout
            // 
            this.bLayout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLayout.ForeColor = System.Drawing.Color.DarkRed;
            this.bLayout.Location = new System.Drawing.Point(15, 180);
            this.bLayout.Margin = new System.Windows.Forms.Padding(2);
            this.bLayout.Name = "bLayout";
            this.bLayout.Size = new System.Drawing.Size(130, 30);
            this.bLayout.TabIndex = 4;
            this.bLayout.Text = "Layout";
            this.bLayout.UseVisualStyleBackColor = true;
            // 
            // bAlignment
            // 
            this.bAlignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bAlignment.ForeColor = System.Drawing.Color.DarkRed;
            this.bAlignment.Location = new System.Drawing.Point(15, 20);
            this.bAlignment.Margin = new System.Windows.Forms.Padding(2);
            this.bAlignment.Name = "bAlignment";
            this.bAlignment.Size = new System.Drawing.Size(130, 30);
            this.bAlignment.TabIndex = 0;
            this.bAlignment.Text = "Alignment";
            this.bAlignment.UseVisualStyleBackColor = true;
            this.bAlignment.Click += new System.EventHandler(this.bAlignment_Click);
            // 
            // bPS_PT
            // 
            this.bPS_PT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bPS_PT.ForeColor = System.Drawing.Color.DarkRed;
            this.bPS_PT.Location = new System.Drawing.Point(15, 219);
            this.bPS_PT.Margin = new System.Windows.Forms.Padding(2);
            this.bPS_PT.Name = "bPS_PT";
            this.bPS_PT.Size = new System.Drawing.Size(130, 30);
            this.bPS_PT.TabIndex = 5;
            this.bPS_PT.Text = "PS/PT";
            this.bPS_PT.UseVisualStyleBackColor = true;
            // 
            // bLoad
            // 
            this.bLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLoad.ForeColor = System.Drawing.Color.DarkRed;
            this.bLoad.Location = new System.Drawing.Point(15, 299);
            this.bLoad.Margin = new System.Windows.Forms.Padding(2);
            this.bLoad.Name = "bLoad";
            this.bLoad.Size = new System.Drawing.Size(130, 30);
            this.bLoad.TabIndex = 7;
            this.bLoad.Text = "Load";
            this.bLoad.UseVisualStyleBackColor = true;
            // 
            // bRebar
            // 
            this.bRebar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bRebar.ForeColor = System.Drawing.Color.DarkRed;
            this.bRebar.Location = new System.Drawing.Point(15, 259);
            this.bRebar.Margin = new System.Windows.Forms.Padding(2);
            this.bRebar.Name = "bRebar";
            this.bRebar.Size = new System.Drawing.Size(130, 30);
            this.bRebar.TabIndex = 6;
            this.bRebar.Text = "Rebar";
            this.bRebar.UseVisualStyleBackColor = true;
            // 
            // bSection
            // 
            this.bSection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bSection.ForeColor = System.Drawing.Color.DarkRed;
            this.bSection.Location = new System.Drawing.Point(15, 60);
            this.bSection.Margin = new System.Windows.Forms.Padding(2);
            this.bSection.Name = "bSection";
            this.bSection.Size = new System.Drawing.Size(130, 30);
            this.bSection.TabIndex = 1;
            this.bSection.Text = "Section";
            this.bSection.UseVisualStyleBackColor = true;
            this.bSection.Click += new System.EventHandler(this.bSection_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(224)))), ((int)(((byte)(0)))));
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(162, 399);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Calculate";
            // 
            // tabInput
            // 
            this.tabInput.Controls.Add(this.tabPage1);
            this.tabInput.Controls.Add(this.tabPage2);
            this.tabInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabInput.Location = new System.Drawing.Point(21, 80);
            this.tabInput.Margin = new System.Windows.Forms.Padding(2);
            this.tabInput.Name = "tabInput";
            this.tabInput.SelectedIndex = 0;
            this.tabInput.Size = new System.Drawing.Size(170, 427);
            this.tabInput.TabIndex = 20;
            this.tabInput.Tag = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Bridge Type";
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "RC";
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "RC",
            "PS",
            "PT"});
            this.comboBox1.Location = new System.Drawing.Point(86, 42);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(101, 23);
            this.comboBox1.TabIndex = 18;
            // 
            // helpAboutToolStripMenuItem
            // 
            this.helpAboutToolStripMenuItem.Name = "helpAboutToolStripMenuItem";
            this.helpAboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.helpAboutToolStripMenuItem.Text = "About CBridge";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpAboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // crossSectionsToolStripMenuItem
            // 
            this.crossSectionsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.crossSectionsToolStripMenuItem.Name = "crossSectionsToolStripMenuItem";
            this.crossSectionsToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.crossSectionsToolStripMenuItem.Text = "Cross Sections...";
            this.crossSectionsToolStripMenuItem.Click += new System.EventHandler(this.crossSectionsToolStripMenuItem_Click);
            // 
            // materialToolStripMenuItem
            // 
            this.materialToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.materialToolStripMenuItem.Name = "materialToolStripMenuItem";
            this.materialToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.materialToolStripMenuItem.Text = "Material...";
            this.materialToolStripMenuItem.Click += new System.EventHandler(this.materialMenu_Click);
            // 
            // componentToolStripMenuItem
            // 
            this.componentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.materialToolStripMenuItem,
            this.crossSectionsToolStripMenuItem});
            this.componentToolStripMenuItem.Name = "componentToolStripMenuItem";
            this.componentToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.componentToolStripMenuItem.Text = "Component";
            // 
            // elementInfoToolStripMenuItem
            // 
            this.elementInfoToolStripMenuItem.Name = "elementInfoToolStripMenuItem";
            this.elementInfoToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.elementInfoToolStripMenuItem.Text = "Element Info";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(138, 6);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newModelToolStripMenuItem,
            this.openBridgeModel,
            this.saveModel,
            this.saveModelAs,
            this.exitCBridgeMenu});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newModelToolStripMenuItem
            // 
            this.newModelToolStripMenuItem.Name = "newModelToolStripMenuItem";
            this.newModelToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.newModelToolStripMenuItem.Text = "New Model";
            // 
            // openBridgeModel
            // 
            this.openBridgeModel.Name = "openBridgeModel";
            this.openBridgeModel.Size = new System.Drawing.Size(151, 22);
            this.openBridgeModel.Text = "Open Model";
            this.openBridgeModel.Click += new System.EventHandler(this.openBridgeModel_Click);
            // 
            // saveModel
            // 
            this.saveModel.Name = "saveModel";
            this.saveModel.Size = new System.Drawing.Size(151, 22);
            this.saveModel.Text = "Save Model";
            this.saveModel.Click += new System.EventHandler(this.saveCBridgeModel_Click);
            // 
            // saveModelAs
            // 
            this.saveModelAs.Name = "saveModelAs";
            this.saveModelAs.Size = new System.Drawing.Size(151, 22);
            this.saveModelAs.Text = "Save Model As";
            this.saveModelAs.Click += new System.EventHandler(this.saveModelAs_Click);
            // 
            // exitCBridgeMenu
            // 
            this.exitCBridgeMenu.Name = "exitCBridgeMenu";
            this.exitCBridgeMenu.Size = new System.Drawing.Size(151, 22);
            this.exitCBridgeMenu.Text = "Exit";
            this.exitCBridgeMenu.Click += new System.EventHandler(this.exitCBridgeMenu_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.componentToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1070, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator1,
            this.elementInfoToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // CBridge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 592);
            this.Controls.Add(this.pbModel);
            this.Controls.Add(this.tabInput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.menuStrip1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CBridge";
            this.Text = "CBridge";
            ((System.ComponentModel.ISupportInitialize)(this.pbModel)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabInput.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbModel;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button bBent;
        private System.Windows.Forms.Button bAbutment;
        private System.Windows.Forms.Button bCodeCheck;
        private System.Windows.Forms.Button bLayout;
        private System.Windows.Forms.Button bAlignment;
        private System.Windows.Forms.Button bPS_PT;
        private System.Windows.Forms.Button bLoad;
        private System.Windows.Forms.Button bRebar;
        private System.Windows.Forms.Button bSection;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem helpAboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crossSectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem componentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elementInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openBridgeModel;
        private System.Windows.Forms.ToolStripMenuItem saveModel;
        private System.Windows.Forms.ToolStripMenuItem saveModelAs;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitCBridgeMenu;
    }
}

