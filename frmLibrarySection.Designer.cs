﻿namespace CBridge
{
    partial class frmLibrarySection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.pbModel = new System.Windows.Forms.PictureBox();
            this.lstBoxViewID = new System.Windows.Forms.ListBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtBrowse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ofdOpen = new System.Windows.Forms.OpenFileDialog();
            this.cmbSelType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbModel)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(426, 357);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Enabled = false;
            this.btnOK.Location = new System.Drawing.Point(345, 357);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 25;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // pbModel
            // 
            this.pbModel.BackColor = System.Drawing.Color.White;
            this.pbModel.Location = new System.Drawing.Point(133, 61);
            this.pbModel.Name = "pbModel";
            this.pbModel.Size = new System.Drawing.Size(368, 278);
            this.pbModel.TabIndex = 24;
            this.pbModel.TabStop = false;
            // 
            // lstBoxViewID
            // 
            this.lstBoxViewID.FormattingEnabled = true;
            this.lstBoxViewID.Location = new System.Drawing.Point(17, 88);
            this.lstBoxViewID.Name = "lstBoxViewID";
            this.lstBoxViewID.Size = new System.Drawing.Size(97, 121);
            this.lstBoxViewID.TabIndex = 21;
            this.lstBoxViewID.SelectedIndexChanged += new System.EventHandler(this.lstBoxViewID_SelectedIndexChanged);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(426, 12);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 19;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtBrowse
            // 
            this.txtBrowse.Location = new System.Drawing.Point(66, 14);
            this.txtBrowse.Name = "txtBrowse";
            this.txtBrowse.ReadOnly = true;
            this.txtBrowse.Size = new System.Drawing.Size(354, 20);
            this.txtBrowse.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Lib. File:";
            // 
            // ofdOpen
            // 
            this.ofdOpen.Filter = "Section CBridgeTools files (*.cbt)|*.cbt";
            // 
            // cmbSelType
            // 
            this.cmbSelType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbSelType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSelType.Enabled = false;
            this.cmbSelType.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbSelType.FormattingEnabled = true;
            this.cmbSelType.Items.AddRange(new object[] {
            "Rectangle",
            "Circle",
            "I-Girder",
            "Box Girder",
            "U-Girder",
            "Precast Box",
            "Voided Slab",
            "CA BathTub",
            "Parts",
            "General"});
            this.cmbSelType.Location = new System.Drawing.Point(17, 61);
            this.cmbSelType.Name = "cmbSelType";
            this.cmbSelType.Size = new System.Drawing.Size(97, 21);
            this.cmbSelType.TabIndex = 83;
            this.cmbSelType.SelectedIndexChanged += new System.EventHandler(this.cmbSelType_SelectedIndexChanged);
            // 
            // frmLibrarySection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 392);
            this.Controls.Add(this.cmbSelType);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.pbModel);
            this.Controls.Add(this.lstBoxViewID);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtBrowse);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLibrarySection";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Library Section";
            this.Load += new System.EventHandler(this.frmLibrarySection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbModel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.PictureBox pbModel;
        private System.Windows.Forms.ListBox lstBoxViewID;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog ofdOpen;
        public System.Windows.Forms.ComboBox cmbSelType;
    }
}