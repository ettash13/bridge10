﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZoconFEA3D;                   // Add for anything that needs to access m_Structure or core functions


namespace CBridge
{
    public partial class frmAlignment : Form
    {
        bool DoEvents = false;   // flag for turning event handling off and on selectively
               
        public Alignment m_Alignment;
        Kanvas2D m_HView;
        Kanvas2D m_VView;
        Kanvas3D m_3DView;

        public bool m_OK = false; // false=return from cancel, true from OK


        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Constructor of Alignment form
        /// 
        /// </summary>
        //////////////////////////////////////////////////////////////////////////////////////////////
        public frmAlignment()
        {
           
            InitializeComponent();

            tabAlignment.SelectedTab = tabHorizontal;

            DoEvents = true; // a flag for turning event handling off and on selectively

        }

        /// <summary>
        /// Moves parameter values into dialog items
        /// </summary>
        public void SetValues(Alignment[] iAlignment)
        {
            if (iAlignment != null)
            {
                UpdateGridView(iAlignment);
                DrawHView();
                DrawVView();

                //bool OK = CheckAllLimits();
                m_OK = true;
                if (!m_OK) btnOK.Enabled = false;
            } 
            else
            {
                // unti we have a good page 
                m_OK = false;
                DoEvents = false;
            }        
        }

        private bool CheckCells()
        {

            DataGridView dgv = null;
            bool AllOK = true;

            dgv = dgvHoizontalSegments;

            for (int iR = 0; iR < dgv.Rows.Count - 1; iR++)
            {
                for (int iC = 0; iC < dgv.ColumnCount - 1; iC++)
                {
                    int  Cellvalue = Convert.ToInt32(dgv.Rows[iR].Cells[iC].Value);

                    if (Cellvalue < 0)
                    {
                        dgv.Rows[iR].Cells[iC].Style.ForeColor = Color.Red;
                        dgv.Rows[iR].Cells[iC].Style.BackColor = Color.Yellow;

                        AllOK = false;
                    }
                }
            }

            return AllOK;
        }

        /// <summary>
        /// Moves parameter valuess into dialog items
        /// </summary>
        public void UpdateGridView(Alignment[] iAlignment)
        {
            DoEvents = false;

            Alignment m_Alignement = new Alignment();

            m_Alignment = iAlignment[0];


            for (int i = 0; i < 6; i++)
            {
                dgvStartXYZ.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            if (m_Alignment != null)
            {
                // Update the StartXYZ Gridview
                dgvStartXYZ["Start_X", 0].Value = m_Alignment.XYZ0[0];
                dgvStartXYZ["Start_Y", 0].Value = m_Alignment.XYZ0[1];
                dgvStartXYZ["Start_Z", 0].Value = m_Alignment.XYZ0[2];

                dgvStartXYZ["AzimuthStart", 0].Value = m_Alignment.CrvAzStart[0]*180/Math.PI;
                dgvStartXYZ["SlopeStart", 0].Value = m_Alignment.m_VSlopeStart;

                // Update the Horizontal curve segments in Gridview
                for (int i = 0; i< m_Alignment.NCurve; i++)
                {
                    // Add a Row
                    i = dgvHoizontalSegments.Rows.Add();

                    // We want to show "-" sign as "Left" direction
                    if (m_Alignment.CrvRadius[i] < 0) 
                    {
                        dgvHoizontalSegments["HDirectionCombo", i].Value = "Left";
                        dgvHoizontalSegments["HDirectionCombo", i].ReadOnly = false;
                    }
                    else if (m_Alignment.CrvRadius[i] > 0)
                    {
                        dgvHoizontalSegments["HDirectionCombo", i].Value = "Right";
                        dgvHoizontalSegments["HDirectionCombo", i].ReadOnly = false;
                    }
                    else
                    {
                        dgvHoizontalSegments["HDirectionCombo", i].Value = "Straight";
                        dgvHoizontalSegments["HDirectionCombo", i].ReadOnly = true;
                        dgvHoizontalSegments["HDirectionCombo", i].Style.ForeColor = Color.Yellow;
                    }

                    dgvHoizontalSegments["Radius", i].Value = Math.Abs(m_Alignment.CrvRadius[i]);
                    dgvHoizontalSegments["Lenght", i].Value = m_Alignment.CrvLength[i];
                }

                // Update the Vertical curve segments in Gridview
                for (int i = 0; i < m_Alignment.m_NVCurve; i++)
                {
                    // Add a Row
                    i = dgvVerticalSegments.Rows.Add();

                    // We want to show "-" sign as "Down" direction
                    if (m_Alignment.m_VCrvRise[i] < 0)
                    {
                        dgvVerticalSegments["VDirectionCombo", i].Value = "Down";
                        dgvVerticalSegments["VDirectionCombo", i].ReadOnly = false;
                    }
                    else if (m_Alignment.m_VCrvRise[i] > 0)
                    {
                        dgvVerticalSegments["VDirectionCombo", i].Value = "Up";
                        dgvVerticalSegments["VDirectionCombo", i].ReadOnly = false;
                    }
                    else 
                    {
                        dgvVerticalSegments["VDirectionCombo", i].Value = "Straight";
                        dgvVerticalSegments["VDirectionCombo", i].ReadOnly = true;
                        dgvHoizontalSegments["HDirectionCombo", i].Style.ForeColor = Color.Yellow;
                    }
                    
                    dgvVerticalSegments["Rise", i].Value = Math.Abs(m_Alignment.m_VCrvRise[i]);
                    dgvVerticalSegments["Run", i].Value = m_Alignment.m_VCrvRun[i];
                }
            }
            DoEvents = true;
        }
        

        ///////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 
        /// Moves Grid view data into structure parameters
        ///  
        /// 
        /// 
        /// </summary>
        ///////////////////////////////////////////////////////////////////////////////////////////////
        public void SaveAlignments()
        {
            // Do one event at a time
            DoEvents = false;

            // will make new object
            m_Alignment = null;

            double[] XYZ0 = new double[3];

            double AzimuthStart = 0;
            double SlopeStart = 0;

            double[] CrvRad;
            double[] CrvLen;
                       
            double[] CrvRise;
            double[] CrvRun;

            if (dgvStartXYZ.Rows.Count > 0)
            {
                XYZ0[0] = Convert.ToDouble(dgvStartXYZ["Start_X", 0].Value);
                XYZ0[1] = Convert.ToDouble(dgvStartXYZ["Start_Y", 0].Value);
                XYZ0[2] = Convert.ToDouble(dgvStartXYZ["Start_Z", 0].Value);

                AzimuthStart = Convert.ToDouble(dgvStartXYZ["AzimuthStart", 0].Value)*Math.PI/180;
                SlopeStart = Convert.ToDouble(dgvStartXYZ["SlopeStart", 0].Value);
            }
            else
            {
                // we have no Start XYZ paramters ?
            }          

            // Get Horizontal segments from Gridview
            int NumHCurves = dgvHoizontalSegments.Rows.Count - 1;

            CrvRad = new double[NumHCurves];
            CrvLen = new double[NumHCurves];
                      
            for (int i = 0; i < NumHCurves; i++)
            {
                // get the radius and lenght
                CrvRad[i] = Convert.ToDouble(dgvHoizontalSegments["Radius", i].Value);
                CrvLen[i] = Convert.ToDouble(dgvHoizontalSegments["Lenght", i].Value);

                // if direction is balnk, start with left
                if (dgvHoizontalSegments["HDirectionCombo", i].Value == null)
                {
                    dgvHoizontalSegments["HDirectionCombo", i].Value = "Right";
                }

                // if direction is left, invert radius sign
                if ((dgvHoizontalSegments["HDirectionCombo", i].Value.ToString()) == "Left" )
                {
                    CrvRad[i] = CrvRad[i] * -1;
                }
            }
            
            // Get Vertical segments from Gridview
            int NumVCurves = dgvVerticalSegments.Rows.Count - 1;

            CrvRise = new double[NumVCurves];
            CrvRun = new double[NumVCurves];
            
            for (int i = 0; i < NumVCurves; i++)
            {
                CrvRise[i] = Convert.ToDouble(dgvVerticalSegments["Rise", i].Value);
                CrvRun[i]  = Convert.ToDouble(dgvVerticalSegments["Run", i].Value);
                
                // if direction is balnk, start with up
                if (dgvVerticalSegments["VDirectionCombo", i].Value == null)
                {      
                    dgvVerticalSegments["VDirectionCombo", i].Value = "Up";
                }
                
                // if direction is down, invert rise sign
                if (dgvVerticalSegments["VDirectionCombo", i].Value == null) dgvVerticalSegments["VDirectionCombo", i].Value = "Up";
                if ((dgvVerticalSegments["VDirectionCombo", i].Value.ToString()) == "Down")
                {
                    CrvRise[i] = CrvRise[i] * -1;
                }
            }
                        
            m_Alignment = new Alignment("A", XYZ0, AzimuthStart, CrvRad, CrvLen);

            m_Alignment.SetVerticalCurve(SlopeStart, CrvRun, CrvRise);
                        
            DoEvents = true;
        }


        private void DrawHView()
        {
            pbHView.Image = null;
            pbHView.Invalidate();
            
            m_HView = new Kanvas2D();
            m_HView.SetWinSize(pbHView.Size.Width, pbHView.Size.Height);
            //
            m_HView.AddPen(new Pen(Color.Red, 1));
            m_HView.AddPen(new Pen(Color.Green, 2));
          
            // Axes
            m_HView.AddLine(0, 0, 100, 0, 2);
            m_HView.AddLine(0, 0, 0, 100, 2);
            
            //Alignment
            double L = m_Alignment.LTotal;

            int nLine = 100;

            double[,] Lines = new double[nLine + 1, 2];
            double dL = L / nLine;

            double X0 = m_Alignment.XYZ0[0];
            double Y0 = m_Alignment.XYZ0[1];

            for (int i = 0; i <= nLine; i++)
            {
                double[] XY = m_Alignment.GetXYZ(i * dL);
                Lines[i, 0] = XY[0];
                Lines[i, 1] = XY[1];
            }
            //
            m_HView.AddPolyLine(Lines, 1);
            m_HView.ZoomFit();
            pbHView.Image = m_HView.GetPicture();
        }

        private void DrawVView()
        {
            pbVView.Image = null;
            pbVView.Invalidate();

            m_VView = new Kanvas2D();
            m_VView.SetWinSize(pbVView.Size.Width, pbVView.Size.Height);
            //
            m_VView.AddPen(new Pen(Color.Red, 1));
            m_VView.AddPen(new Pen(Color.Green, 2));

            // Axes
            m_VView.AddLine(0, 0, 100, 0, 2);
            m_VView.AddLine(0, 0, 0, 100, 2);

            //Alignment
            double L = m_Alignment.LTotal;

            int nLine = 100;

            double[,] Lines = new double[nLine + 1, 2];
            double dL = L / nLine;

            double X0 = m_Alignment.XYZ0[0];
            double Y0 = m_Alignment.XYZ0[2];

            for (int i = 0; i <= nLine; i++)
            {
                double[] XY = m_Alignment.GetXYZ(i * dL);
                Lines[i, 0] = i * dL;
                Lines[i, 1] = XY[2];
            }
            //
            m_VView.AddPolyLine(Lines, 1);
            m_VView.ZoomFit();
            pbVView.Image = m_VView.GetPicture();
        }


        private void Draw3DView()
        {
            pb3DView.Image = null;
            pb3DView.Invalidate();

            m_3DView = new Kanvas3D();
            m_3DView.SetWinSize(pb3DView.Size.Width, pb3DView.Size.Height);
            //
            m_3DView.AddPen(new Pen(Color.Red, 1));
            m_3DView.AddPen(new Pen(Color.Green, 2));

            m_3DView.AddLine(0, 0, 0, 100, 2);

            // Axes
            double[] X = { 0, 0, 0, 100, 0, 0 };
            m_3DView.AddLine(X, 2);
            double[] Y = { 0, 0, 0, 0, 1000, 0 };
            m_3DView.AddLine(Y, 2);
            double[] Z = { 0, 0, 0, 0, 0, 100 };
            m_3DView.AddLine(Z, 2);

            //Alignment
            double L = m_Alignment.LTotal;

            int nLine = 100;

            double[,] Lines = new double[nLine + 1, 3];
            double dL = L / nLine;

            double X0 = m_Alignment.XYZ0[0];
            double Y0 = m_Alignment.XYZ0[1];
            double Z0 = m_Alignment.XYZ0[2];

            for (int i = 0; i <= nLine; i++)
            {
                double[] XYZ = m_Alignment.GetXYZ(i * dL);
                Lines[i, 0] = XYZ[0];
                Lines[i, 1] = XYZ[1];
                Lines[i, 2] = XYZ[2];
            }
            //
            m_3DView.AddPolyLine(Lines, 1);
            m_3DView.RotAngle(0, 45,45,45);
            m_3DView.ZoomFit();
            m_3DView.ResetK2D();
            pb3DView.Image = m_3DView.GetPicture();
        }

          
        private void dgvHoizontalSegments_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            // Check all cell value to be positive number
            // if radius is not zero unlock the direction combo box
            // if radius is not zero unlock the direction combo box 

            int test = 1;

            if (test == 1)
            {
                return;
            }
        }

        private void btnReplicate_Click(object sender, EventArgs e)
        {
            if (dgvHoizontalSegments.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dgvHoizontalSegments.SelectedRows)
                {
                    ReplicateHorizontalRow(item.Index);
                }
            }

            else if (dgvVerticalSegments.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dgvVerticalSegments.SelectedRows)
                {
                    ReplicateVerticalRow(item.Index);
                }
            }
        }

        private void ReplicateHorizontalRow(int RIndex)
        {
            DoEvents = false;

            int iRow = dgvHoizontalSegments.Rows.Add(); // The newly aded row index is the last row

            for (int j = 0; j < 3; j++)
            {
                dgvHoizontalSegments[j, iRow].Value = dgvHoizontalSegments[j, RIndex].Value;
            }

            DoEvents = true;
        }

        private void ReplicateVerticalRow(int RIndex)
        {
          
            DoEvents = false;
                     
            int iRow = dgvVerticalSegments.Rows.Add(); // The newly aded row index is the last row

            for (int j = 0; j < 3; j++)
            {
                dgvVerticalSegments[j, iRow].Value = dgvVerticalSegments[j, RIndex].Value;
            }
       
            DoEvents = true;
        }
        

        private void btnDeleteRow_Click(object sender, EventArgs e)
        {
            if (dgvHoizontalSegments.SelectedRows.Count > 0)
            {

                foreach (DataGridViewRow item in dgvHoizontalSegments.SelectedRows)
                {
                    dgvHoizontalSegments.Rows.Remove(item);
                }
            }
            else if (dgvVerticalSegments.SelectedRows.Count > 0)
            {

                foreach (DataGridViewRow item in dgvVerticalSegments.SelectedRows)
                {
                    dgvVerticalSegments.Rows.Remove(item);
                }
            }
                        
        }



        private void btnOK_Click(object sender, EventArgs e)
        {
            // update the Alignment parameters
            SaveAlignments();

            m_OK = true;
            this.Close();
        }


        public void UpdateBridgeInfo(ref Alignment[] iAlignment)
        {

            iAlignment = new Alignment[1];

            iAlignment[0] = m_Alignment;
        }
        

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            //  CheckData();
            //UpdateData();
            DrawHView();
            DrawVView();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_OK = false;
            this.Close();
            return;
        }


        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                //  CheckData();
                SaveAlignments();
                DrawHView();
                DrawVView();
            }
        }

        private void pbHView_SizeChanged(object sender, EventArgs e)
        {
            DrawHView();
            DrawVView();
        }

        private void dgv_CellValueChanged(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (DoEvents)
            {
                //CheckData();
                SaveAlignments();
                DrawHView();
                DrawVView();
            }
        }

        private void tabAlignment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CheckData();
            SaveAlignments();
            DrawHView();
            DrawVView();
            Draw3DView();
        }
        
        private void dgvHoizontalSegments_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                CheckCells();
                SaveAlignments();
                DrawHView();
            }
        }

        private void dgvVerticalSegments_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                //CheckData();
                SaveAlignments();
                DrawVView();
            }
        }
    }
}