﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using ZDraw;
using ZoconFEA3D;

namespace CBridge
{
    public partial class frmProperties : Form
    {
        string strUnit;
        public string getType;
        public double Prop0, Prop1, Prop2, Prop3, Prop4, Prop5, Prop6, Prop7;
        public CrossSection STemp = new CrossSection();

        public frmProperties()
        {
            InitializeComponent();
        }

        private void frmProperties_Load(object sender, EventArgs e)
        {
            cmbUnit.Text = "in";

            dgvProp.RowCount = 8;

            dgvProp[0, 0].Value = "A, in^2";
            dgvProp[0, 1].Value = "Ay, in^2";
            dgvProp[0, 2].Value = "Az, in^2";
            dgvProp[0, 3].Value = "J, in^4";
            dgvProp[0, 4].Value = "Iy, in^4";
            dgvProp[0, 5].Value = "Iz, in^4";
            dgvProp[0, 6].Value = "H, in";
            dgvProp[0, 7].Value = "Ytop, in";

            dgvProp[1, 0].Value = Prop0;
            dgvProp[1, 1].Value = Prop1;
            dgvProp[1, 2].Value = Prop2;
            dgvProp[1, 3].Value = Prop3;
            dgvProp[1, 4].Value = Prop4;
            dgvProp[1, 5].Value = Prop5;
            dgvProp[1, 6].Value = Prop6;
            dgvProp[1, 7].Value = Prop7;

            dgvProp.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvProp.AllowUserToResizeRows = false;
        }

        private void cmbUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            strUnit = cmbUnit.Text;
        }

        private void cmbUnit_SelectedValueChanged(object sender, EventArgs e)
        {
            if (strUnit != cmbUnit.Text)
            {
                switch (strUnit)
                {
                    case "in":
                        dgvProp[0, 0].Value = "A, ft^2";
                        dgvProp[0, 1].Value = "Ay, ft^2";
                        dgvProp[0, 2].Value = "Az, ft^2";
                        dgvProp[0, 3].Value = "J, ft^4";
                        dgvProp[0, 4].Value = "Iy, ft^4";
                        dgvProp[0, 5].Value = "Iz, ft^4";
                        dgvProp[0, 6].Value = "H, ft";
                        dgvProp[0, 7].Value = "Ytop, ft";

                        dgvProp[1, 0].Value = (Convert.ToDouble(dgvProp[1, 0].Value)) / 144;
                        dgvProp[1, 1].Value = (Convert.ToDouble(dgvProp[1, 1].Value)) / 144;
                        dgvProp[1, 2].Value = (Convert.ToDouble(dgvProp[1, 2].Value)) / 144;
                        dgvProp[1, 3].Value = (Convert.ToDouble(dgvProp[1, 3].Value)) / 20736;
                        dgvProp[1, 4].Value = (Convert.ToDouble(dgvProp[1, 4].Value)) / 20736;
                        dgvProp[1, 5].Value = (Convert.ToDouble(dgvProp[1, 5].Value)) / 20736;
                        dgvProp[1, 6].Value = (Convert.ToDouble(dgvProp[1, 6].Value)) / 12;
                        dgvProp[1, 7].Value = (Convert.ToDouble(dgvProp[1, 7].Value)) / 12;
                        
                        //for (int i = 0; i <= 7; i++)
                        //{
                        //    dgvProp[1, i].Value = (Convert.ToDouble(dgvProp[1, i].Value)) / 12;
                        //}
                        break;
                    case "ft":
                        dgvProp[0, 0].Value = "A, in^2";
                        dgvProp[0, 1].Value = "Ay, in^2";
                        dgvProp[0, 2].Value = "Az, in^2";
                        dgvProp[0, 3].Value = "J, in^4";
                        dgvProp[0, 4].Value = "Iy, in^4";
                        dgvProp[0, 5].Value = "Iz, in^4";
                        dgvProp[0, 6].Value = "H, in";
                        dgvProp[0, 7].Value = "Ytop, in";

                        dgvProp[1, 0].Value = (Convert.ToDouble(dgvProp[1, 0].Value)) * 144;
                        dgvProp[1, 1].Value = (Convert.ToDouble(dgvProp[1, 1].Value)) * 144;
                        dgvProp[1, 2].Value = (Convert.ToDouble(dgvProp[1, 2].Value)) * 144;
                        dgvProp[1, 3].Value = (Convert.ToDouble(dgvProp[1, 3].Value)) * 20736;
                        dgvProp[1, 4].Value = (Convert.ToDouble(dgvProp[1, 4].Value)) * 20736;
                        dgvProp[1, 5].Value = (Convert.ToDouble(dgvProp[1, 5].Value)) * 20736;
                        dgvProp[1, 6].Value = (Convert.ToDouble(dgvProp[1, 6].Value)) * 12;
                        dgvProp[1, 7].Value = (Convert.ToDouble(dgvProp[1, 7].Value)) * 12;
                        break;
                }
            }
        }

    }
}
