﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZoconFEA3D;
//using ZDraw;

namespace CBridge
{
    public partial class frmSection : Form
    {
        public CrossSection STemp = new CrossSection();
        Kanvas2D m_View;

        bool DoEvents = false; // a flag for turning event handling off and on selectively
        //int m_Unit = 0; //Correponds to cmbUnit index for kip, inch
        string[,] m_HeaderText = new string[1, 4]; // [nunits, ncolumns]
        //double[,] m_UnitFactor = null; // unit conversions from ksi to selected unit
        //double[,] m_Limits = null; //  Min, max, default, unit factor for each column
        public CrossSection[] m_SectionList; // The incoming data to dialog; in kip & inch
        bool[] m_InUse; // Flags for each row being is use or not
        //  public Material[] m_MaterialListNew; // The Outgoing data from dialog; in kip & inch
        public bool m_OK = false; // false=return from cancel, true from OK
        public frmSection()
        {
            InitializeComponent();
            //
            //m_Limits = new double[1, 5];  //  Min, max, default, unit conversion for each column (incoming units are kip, in, degree F) {InternalName, DisplayName, ...)
            //int i = 3;
            //m_Limits[0, i] = 1.0e-10; m_Limits[1, i] = 1.0e+15; m_Limits[2, i] = 30000; ; m_Limits[3, i] = 1; i++; // E ksi
            //m_Limits[0, i] = 1.0e-10; m_Limits[1, i] = 1.0; m_Limits[2, i] = 0.3; ; m_Limits[3, i] = 1; i++; // Poisson Ratio unitless
            //m_Limits[0, i] = 1.0e-10; m_Limits[1, i] = 0.01; m_Limits[2, i] = 1.0e-6; ; m_Limits[3, i] = 1; i++; // Thermal Coeff. 1/degF
            //m_Limits[0, i] = 0; m_Limits[1, i] = 1.0e+15; m_Limits[2, i] = 490; ; m_Limits[3, i] = 1; i++; // Unit Weight pcf (kci to pcf)
            // Header Texts
            //string[,] HeaderText1 = {{"E (psi)", "nu", "Alpha(/oF)", "gamma (pci)"},
            //                            {"E (psf)", "nu", "Alpha(/oF)", "gamma (pcf)"},
            //                            {"E (ksi)", "nu", "Alpha(/oF)", "gamma (kci)"},
            //                            {"E (ksf)", "nu", "Alpha(/oF)", "gamma (kcf)"}};
            //double[,] factor1 = {{1000.0, 1.0, 1.0, 1000.0},
            //                        {(1000*144), 1.0, 1.0, (1000*1728)},
            //                        {1.0, 1.0, 1.0, 1.0},
            //                        {144.0, 1.0, 1.0, 1728.0}};
            //m_HeaderText = HeaderText1;
            //m_UnitFactor = factor1;
            //cmbUnit.Text = cmbUnit.Items[2].ToString(); // Initialize the dialog to kip, in
            //cmbUnit.SelectedIndex = 2;
            //m_Unit = 2;
            //for (int j = 3; j < 7; j++) dgvMaterial.Columns[j].HeaderText = m_HeaderText[m_Unit, j - 3];
            DoEvents = true; // a flag for turning event handling off and on selectively
        }
        // --------------------------------------------------------------------
        // User Functions
        // --------------------------------------------------------------------
        public void SetSectionList(CrossSection[] SectionList, bool[] InUse)
        {
            m_SectionList = SectionList;
            m_InUse = InUse;
            InitializeDialog();
        }
        /// <summary>
        /// Moves parameter valuess into dialog items
        /// </summary>
        public void InitializeDialog()
        {
            if (m_SectionList != null && m_SectionList.Length > 0)
            {
                DoEvents = false;
                int nItems = 0;
                for (int i = 0; i < m_SectionList.Length; i++)
                {
                    if (m_SectionList[i] != null) nItems++;
                }
                if (nItems > 0)
                {
                    dgvSection.Rows.Add(nItems);
                    for (int i = 0; i < nItems; i++)
                    {
                        dgvSection["InternalName", i].Value = m_SectionList[i].CrossSecName;
                        dgvSection["DisplayName", i].Value = m_SectionList[i].DisplayName;
                        if (m_InUse[i]) dgvSection["InUse", i].Value = "Yes"; else dgvSection["InUse", i].Value = "No";  // Yes/No for In-Use
                        dgvSection["Type", i].Value = m_SectionList[i].SecShape();
                    }
                    dgvSection.Select();
                }
                DoEvents = true;
            }
        }
        private bool CheckLimits(int Row, int Column, string Value, ref string Message)
        {
            bool RetCode = true;
            try
            {
                int caseCode = 0; // Nothing
                if (Column == 1) caseCode = 1; // Name
                if (Column > 2 && Column < 7) caseCode = 2; // Limits
                switch (caseCode)
                {
                    case 1:  // Check for duplicate names
                        for (int i = 0; i < dgvSection.Rows.Count - 1; i++)
                        {
                            string CheckName = dgvSection[1, i].Value.ToString();
                            if (Value == CheckName && i != Row)
                            {
                                RetCode = false;
                                Message = "Duplicate Name";
                                break;
                            }
                        }
                        break;
                    case 2:  // Check Limits - Nothing to check
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                RetCode = false;
                Message = "Limit Check Exception";
            }
            return RetCode;
        }
        /// <summary>
        /// Moves dialog data into parameters
        /// </summary>
        protected void UpdateSection(CrossSection Sec, int Row)
        {
            if (Row >= m_SectionList.Length)
            {
                if (Row > m_SectionList.Length) throw new Exception("UpadteSection Internal Error: Too many sections at one time");
                CrossSection[] NewList = new CrossSection[Row + 1];
                for (int i = 0; i < m_SectionList.Length; i++)
                {
                    NewList[i] = m_SectionList[i];
                }
                m_SectionList = NewList;
            }
            m_SectionList[Row] = Sec;
            if (dgvSection["InUSe",Row].Value.ToString() == "No") AssignSecName(m_SectionList, Row);
        }
        protected void AssignSecName(CrossSection[] SectionList, int item)
        {
            if (SectionList.Length == 0) return;
            if (SectionList.Length == 1)
            {
                SectionList[0].CrossSecName = "S0";
            }
            else
            {
                string BaseName = "S0";
                string NewName = GetName(BaseName);
                bool NameOK = false;
                while (!NameOK)
                {
                    NewName = GetName(BaseName);
                    NameOK = true;
                    for (int i = 0; i < m_SectionList.Length; i++)
                    {
                        if (i != item && NewName == m_SectionList[i].CrossSecName)
                        {
                            NameOK = false;
                            BaseName = NewName;
                            break;
                        }
                    }
                }
            }
        }
        private string GetName(string BaseName)
        {
        IncrementName:
            string NewName = BaseName.Substring(0, BaseName.Length - 1);
            string right = BaseName.Substring(BaseName.Length - 1); // rightmost charachter
            try
            {
                int n = Convert.ToInt16(right);
                if (n == 9) NewName = NewName + "10";
                else NewName = NewName + Convert.ToString(n + 1);
            }
            catch
            {
                NewName = BaseName + "0";
            }
            for (int i = 0; i < dgvSection.Rows.Count - 2; i++) // Check for duplicate names
            {
                if (dgvSection[1, i].Value.ToString() == NewName)
                {
                    BaseName = NewName;
                    goto IncrementName;
                }
            }
            return NewName;
        }

        private void ReplicateRow(int RIndex)
        {
            bool EventStatus = DoEvents;
            DoEvents = false;
            dgvSection.Rows.Add();
            int iRow = dgvSection.Rows.Count - 2;  // The newly aded row index is the last row
            string baseName = dgvSection[1, RIndex].Value.ToString();
            string newName = GetName(baseName);
            dgvSection[1, iRow].Value = newName;
            dgvSection[2, iRow].Value = "No";
            dgvSection[4, iRow].Value = "Modify";
            dgvSection[5, iRow].Value = "Properties";
            CrossSection newSec = new CrossSection(m_SectionList[RIndex]); // copy the section
            dgvSection[3, iRow].Value = newSec.SecShape();
            UpdateSection(newSec, iRow);
            DoEvents = EventStatus;
        }

        private void RemoveSectionFromList(ref CrossSection[] List, int item){
            int L = List.Length;
            CrossSection[] newList = new CrossSection[L-1];
            for (int i=0;i<item;i++) newList[i] = List[i];
            for (int i=item+1; i<L;i++) newList[i-1] = List[i];
            List = newList;
        }

        // --------------------------------------------------------------------
        // Event Functions
        // --------------------------------------------------------------------

        private void dgvSection_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DoEvents)
            {
                if (dgvSection.CurrentRow == null) return;
                string Message = "";
                int Row = dgvSection.CurrentRow.Index;
                int Col = dgvSection.CurrentCell.ColumnIndex;
                string Cellvalue = Convert.ToString(dgvSection.CurrentCell.Value);
                if (!CheckLimits(Row, Col, Cellvalue, ref Message))
                {
                    dgvSection.Rows[Row].Cells[Col].Style.ForeColor = Color.Red;
                    bOK.Enabled = false;
                }
                else
                {
                    Message = "";
                    bool AllOK = true;
                    for (int iR = 0; iR < dgvSection.Rows.Count - 1; iR++)
                    {
                        for (int iC = 1; iC < dgvSection.ColumnCount - 1; iC++)
                        {
                            if (iC != 2) // do not check the In-Use Column
                            {
                                Cellvalue = Convert.ToString(dgvSection.Rows[iR].Cells[iC].Value);
                                if (!CheckLimits(iR, iC, Cellvalue, ref Message))
                                {
                                    AllOK = false;
                                }
                                else
                                {
                                    dgvSection.Rows[iR].Cells[iC].Style.ForeColor = Color.Black;
                                }
                            }
                        }
                    }
                    if (AllOK)
                    {
                        bOK.Enabled = true;
                    }
                }
            }
        }

        private void dgvSection_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (DoEvents)
            {
                DoEvents = false;
                int i = dgvSection.CurrentRow.Index;
                dgvSection["DisplayName", i].Value = GetName("New");
                dgvSection[2, i].Value = "No";
                CrossSection Sec0 = null;
                if (i == 0)
                {
                    Sec0 = new CrossSection();
                    Sec0.SetSectionShape("Rectangle", new double[] { 1, 1 }, false, null, false);
                    Sec0.DisplayName = dgvSection["DisplayName", i].Value.ToString();
                    m_SectionList = new CrossSection[0];
                    UpdateSection(Sec0, 0);
                    //m_SectionList[0] = Sec0;
                }
                else
                {
                    Sec0 = new CrossSection(m_SectionList[i-1]);
                    UpdateSection(Sec0, i);
                }
                dgvSection[2, i].Value = "No";
                dgvSection[3, i].Value = Sec0.SecShape();                
                DoEvents = true;
                Draw();
            }
        }

        private void dgvSection_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvSection.SelectedRows.Count > 0)
            {
                bDelete.Enabled = true;
                bReplicate.Enabled = true;
            }
            else
            {
                bDelete.Enabled = false;
                bReplicate.Enabled = false;
            }
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            int ii = dgvSection.SelectedRows.Count;
            foreach (DataGridViewRow item in dgvSection.SelectedRows)
            {
                if (item.Cells[2].Value.ToString() == "No")
                {
                    int iRow = -1;
                    for (int i = 0; i < dgvSection.Rows.Count; i++)
                    {
                        if (item == dgvSection.Rows[i])
                        {
                            iRow = i;
                            break;
                        }
                    }
                    RemoveSectionFromList(ref m_SectionList, iRow);
                    dgvSection.Rows.Remove(item);
                }
            }
        }

        private void bReplicate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgvSection.SelectedRows)
            {
                ReplicateRow(item.Index);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            m_OK = false;
            this.Close();
            return;
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            // Reset the Section List
            // UpdateData();
            //Close;
            for (int i = 0; i < dgvSection.Rows.Count - 1; i++)
            {
                m_SectionList[i].DisplayName = dgvSection["DisplayName", i].Value.ToString();
            }
            m_OK = true;
            this.Close();
            return;
        }

        private void dgvSection_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int iRow = dgvSection.CurrentCell.RowIndex;
            if (iRow == dgvSection.Rows.Count-1) return;
            CrossSection Sec = m_SectionList[iRow];

            if (dgvSection.CurrentCell.ColumnIndex == 4)
            {
                frmMemberSections fMemberSections = new frmMemberSections();
                fMemberSections.SetSection(Sec);
                fMemberSections.ShowDialog();
                if (fMemberSections.m_OK)
                {
                    m_SectionList[iRow] = fMemberSections.STemp;
                    dgvSection[3, iRow].Value = m_SectionList[iRow].SecShape();
                }
            }

            if (dgvSection.CurrentCell.ColumnIndex == 5)
            {
                int getRow = dgvSection.CurrentCell.RowIndex;

                frmProperties fProperties = new frmProperties();
                fProperties.getType = m_SectionList[getRow].SecShape();

                switch (m_SectionList[getRow].SecShape())
                {
                    case "Rectangle":
                        int iR = getRow;
                        double[] RecSecDims = m_SectionList[iR].getShapeDims();
                        double[] dimsRec = new double[2];
                        dimsRec[0] = Convert.ToDouble(RecSecDims[0]);
                        dimsRec[1] = Convert.ToDouble(RecSecDims[1]);
                        STemp.SetSectionShape("Rectangle", dimsRec, false, null, true);
                        fProperties.Prop0 = STemp.area;
                        fProperties.Prop1 = STemp.area22;
                        fProperties.Prop2 = STemp.area33;
                        fProperties.Prop3 = STemp.J;
                        fProperties.Prop4 = STemp.I22;
                        fProperties.Prop5 = STemp.I33;
                        fProperties.Prop6 = STemp.DepthZ;
                        fProperties.Prop7 = STemp.CGZ;
                        break;
                    case "Circle":
                        int iC = getRow;
                        double[] CirSecDims = m_SectionList[iC].getShapeDims();
                        double[] dimsCir = new double[1];
                        dimsCir[0] = Convert.ToDouble(CirSecDims[0]);
                        STemp.SetSectionShape("Circle", dimsCir, false, null, true);
                        fProperties.Prop0 = STemp.area;
                        fProperties.Prop1 = STemp.area22;
                        fProperties.Prop2 = STemp.area33;
                        fProperties.Prop3 = STemp.J;
                        fProperties.Prop4 = STemp.I22;
                        fProperties.Prop5 = STemp.I33;
                        fProperties.Prop6 = STemp.DepthZ;
                        fProperties.Prop7 = STemp.CGZ;
                        break;
                    case "I_Girder":
                        int iG = getRow;
                        double[] dimsIG = new double[10];
                        double[] I_GirderSecDims = m_SectionList[iG].getShapeDims();
                        dimsIG[0] = Convert.ToDouble(I_GirderSecDims[0]);
                        dimsIG[1] = Convert.ToDouble(I_GirderSecDims[1]);
                        dimsIG[2] = Convert.ToDouble(I_GirderSecDims[2]);
                        dimsIG[3] = Convert.ToDouble(I_GirderSecDims[3]);
                        dimsIG[4] = Convert.ToDouble(I_GirderSecDims[4]);
                        dimsIG[5] = Convert.ToDouble(I_GirderSecDims[5]);
                        dimsIG[6] = Convert.ToDouble(I_GirderSecDims[6]);
                        dimsIG[7] = Convert.ToDouble(I_GirderSecDims[7]);
                        dimsIG[8] = Convert.ToDouble(I_GirderSecDims[8]);
                        dimsIG[9] = Convert.ToDouble(I_GirderSecDims[9]);
                        STemp.SetSectionShape("I_Girder", dimsIG, false, null, true);
                        fProperties.Prop0 = STemp.area;
                        fProperties.Prop1 = STemp.area22;
                        fProperties.Prop2 = STemp.area33;
                        fProperties.Prop3 = STemp.J;
                        fProperties.Prop4 = STemp.I22;
                        fProperties.Prop5 = STemp.I33;
                        fProperties.Prop6 = STemp.DepthZ;
                        fProperties.Prop7 = STemp.CGZ;
                        break;
                    case "Box_Girder":
                        int iB = getRow;
                        double[] dimsBG = new double[20];
                        double[] Box_GirderSecDims = m_SectionList[iB].getShapeDims();
                        dimsBG[0] = Convert.ToDouble(Box_GirderSecDims[0]);
                        dimsBG[1] = Convert.ToDouble(Box_GirderSecDims[1]);
                        dimsBG[2] = Convert.ToDouble(Box_GirderSecDims[2]);
                        dimsBG[3] = Convert.ToDouble(Box_GirderSecDims[3]);
                        dimsBG[4] = Convert.ToDouble(Box_GirderSecDims[4]);
                        dimsBG[5] = Convert.ToDouble(Box_GirderSecDims[5]);
                        dimsBG[6] = Convert.ToDouble(Box_GirderSecDims[6]);
                        dimsBG[7] = Convert.ToDouble(Box_GirderSecDims[7]);
                        dimsBG[8] = Convert.ToDouble(Box_GirderSecDims[8]);
                        dimsBG[9] = Convert.ToDouble(Box_GirderSecDims[9]);
                        dimsBG[10] = Convert.ToDouble(Box_GirderSecDims[10]);
                        dimsBG[11] = Convert.ToDouble(Box_GirderSecDims[11]);
                        dimsBG[12] = Convert.ToDouble(Box_GirderSecDims[12]);
                        dimsBG[13] = Convert.ToDouble(Box_GirderSecDims[13]);
                        dimsBG[14] = Convert.ToDouble(Box_GirderSecDims[14]);
                        dimsBG[15] = Convert.ToDouble(Box_GirderSecDims[15]);
                        dimsBG[16] = Convert.ToDouble(Box_GirderSecDims[16]);
                        dimsBG[17] = Convert.ToDouble(Box_GirderSecDims[17]);
                        dimsBG[18] = Convert.ToDouble(Box_GirderSecDims[18]);
                        dimsBG[19] = Convert.ToDouble(Box_GirderSecDims[19]);
                        STemp.SetSectionShape("Box_Girder", dimsBG, false, null, true);
                        fProperties.Prop0 = STemp.area;
                        fProperties.Prop1 = STemp.area22;
                        fProperties.Prop2 = STemp.area33;
                        fProperties.Prop3 = STemp.J;
                        fProperties.Prop4 = STemp.I22;
                        fProperties.Prop5 = STemp.I33;
                        fProperties.Prop6 = STemp.DepthZ;
                        fProperties.Prop7 = STemp.CGZ;
                        break;
                    case "U_Girder":
                        int iU = getRow;
                        double[] dimsUG = new double[9];
                        double[] U_GirderSecDims = m_SectionList[iU].getShapeDims();
                        dimsUG[0] = Convert.ToDouble(U_GirderSecDims[0]);
                        dimsUG[1] = Convert.ToDouble(U_GirderSecDims[1]);
                        dimsUG[2] = Convert.ToDouble(U_GirderSecDims[2]);
                        dimsUG[3] = Convert.ToDouble(U_GirderSecDims[3]);
                        dimsUG[4] = Convert.ToDouble(U_GirderSecDims[4]);
                        dimsUG[5] = Convert.ToDouble(U_GirderSecDims[5]);
                        dimsUG[6] = Convert.ToDouble(U_GirderSecDims[6]);
                        dimsUG[7] = Convert.ToDouble(U_GirderSecDims[7]);
                        dimsUG[8] = Convert.ToDouble(U_GirderSecDims[8]);
                        STemp.SetSectionShape("U_Girder", dimsUG, false, null, true);
                        fProperties.Prop0 = STemp.area;
                        fProperties.Prop1 = STemp.area22;
                        fProperties.Prop2 = STemp.area33;
                        fProperties.Prop3 = STemp.J;
                        fProperties.Prop4 = STemp.I22;
                        fProperties.Prop5 = STemp.I33;
                        fProperties.Prop6 = STemp.DepthZ;
                        fProperties.Prop7 = STemp.CGZ;
                        break;
                    case "PC_Box":
                        int iP = getRow;
                        double[] dimsPB = new double[6];
                        double[] PC_BoxSecDims = m_SectionList[iP].getShapeDims();
                        dimsPB[0] = Convert.ToDouble(PC_BoxSecDims[0]);
                        dimsPB[1] = Convert.ToDouble(PC_BoxSecDims[1]);
                        dimsPB[2] = Convert.ToDouble(PC_BoxSecDims[2]);
                        dimsPB[3] = Convert.ToDouble(PC_BoxSecDims[3]);
                        dimsPB[4] = Convert.ToDouble(PC_BoxSecDims[4]);
                        dimsPB[5] = Convert.ToDouble(PC_BoxSecDims[5]);
                        STemp.SetSectionShape("PC_Box", dimsPB, false, null, true);
                        fProperties.Prop0 = STemp.area;
                        fProperties.Prop1 = STemp.area22;
                        fProperties.Prop2 = STemp.area33;
                        fProperties.Prop3 = STemp.J;
                        fProperties.Prop4 = STemp.I22;
                        fProperties.Prop5 = STemp.I33;
                        fProperties.Prop6 = STemp.DepthZ;
                        fProperties.Prop7 = STemp.CGZ;
                        break;
                }
                fProperties.ShowDialog();

                //frmProperties fProperties = new frmProperties();
                //fProperties.SetSection(Sec);
                //fProperties.ShowDialog();
                //if (Properties.m_OK)
                //{
                //    m_SectionList[iRow] = Properties.STemp;
                //    dgvSection[3, iRow].Value = m_SectionList[iRow].SecShape();
                //}
            }
            
            Draw();
            }

        private void Draw()
        {
            int getRow = dgvSection.CurrentCell.RowIndex;
            switch (m_SectionList[getRow].SecShape())
            {

                case "Rectangle":
                    int iR = getRow;
                    double[] RecSecDims = m_SectionList[iR].getShapeDims();
                    m_View = new Kanvas2D();
                    m_View.SetWinSize(pbSection.Size.Width, pbSection.Size.Height);
                    m_View.AddPen(new Pen(Color.Green));
                    try
                    {
                        double[] dims = new double[2];
                        dims[0] = RecSecDims[0];
                        dims[1] = RecSecDims[1];
                        double[,] XY;
                        XY = STemp.SecPointsByShape("Rectangle", dims, false, null);
                        m_View.AddPolygon(XY, 1, 0);
                        m_View.ZoomFit();
                        pbSection.Image = m_View.GetPicture();
                    }
                    catch
                    {
                        MessageBox.Show("Input value was not in a correct format.", "Error");
                    }
                    break;
                case "Circle":
                    int iC = getRow;
                    double[] CirSecDims = m_SectionList[iC].getShapeDims();
                    m_View = new Kanvas2D();
                    m_View.SetWinSize(pbSection.Size.Width, pbSection.Size.Height);
                    m_View.AddPen(new Pen(Color.Green));
                    try
                    {
                        double[] dims = new double[1];
                        dims[0] = CirSecDims[0];
                        double[,] XY;
                        XY = STemp.SecPointsByShape("Circle", dims, false, null);
                        m_View.AddPolygon(XY, 1, 0);
                        m_View.ZoomFit();
                        pbSection.Image = m_View.GetPicture();
                    }
                    catch
                    {
                        MessageBox.Show("Input value was not in a correct format.", "Error");
                    }
                    break;
                case "I_Girder":
                    int iG = getRow;
                    double[] I_GirderSecDims = m_SectionList[iG].getShapeDims();
                    m_View = new Kanvas2D();
                    m_View.SetWinSize(pbSection.Size.Width, pbSection.Size.Height);
                    m_View.AddPen(new Pen(Color.Green));
                    try
                    {
                        double[] dims = new double[10];
                        dims[0] = I_GirderSecDims[0];
                        dims[1] = I_GirderSecDims[1];
                        dims[2] = I_GirderSecDims[2];
                        dims[3] = I_GirderSecDims[3];
                        dims[4] = I_GirderSecDims[4];
                        dims[5] = I_GirderSecDims[5];
                        dims[6] = I_GirderSecDims[6];
                        dims[7] = I_GirderSecDims[7];
                        dims[8] = I_GirderSecDims[8];
                        dims[9] = I_GirderSecDims[9];
                        double[,] XY;
                        XY = STemp.SecPointsByShape("I_Girder", dims, false, null);
                        m_View.AddPolygon(XY, 1, 0);
                        m_View.ZoomFit();
                        pbSection.Image = m_View.GetPicture();
                    }
                    catch
                    {
                        MessageBox.Show("Input value was not in a correct format.", "Error");
                    }

                    break;
                case "Box_Girder":
                    int iB = getRow;
                    double[] Box_GirderSecDims = m_SectionList[iB].getShapeDims();
                    m_View = new Kanvas2D();
                    m_View.SetWinSize(pbSection.Size.Width, pbSection.Size.Height);
                    m_View.AddPen(new Pen(Color.Green));
                    try
                    {
                        double[] dims = new double[20];
                        dims[0] = Box_GirderSecDims[0];
                        dims[1] = Box_GirderSecDims[1];
                        dims[2] = Box_GirderSecDims[2];
                        dims[3] = Box_GirderSecDims[3];
                        dims[4] = Box_GirderSecDims[4];
                        dims[5] = Box_GirderSecDims[5];
                        dims[6] = Box_GirderSecDims[6];
                        dims[7] = Box_GirderSecDims[7];
                        dims[8] = Box_GirderSecDims[8];
                        dims[9] = Box_GirderSecDims[9];
                        dims[10] = Box_GirderSecDims[10];
                        dims[11] = Box_GirderSecDims[11];
                        dims[12] = Box_GirderSecDims[12];
                        dims[13] = Box_GirderSecDims[13];
                        dims[14] = Box_GirderSecDims[14];
                        dims[15] = Box_GirderSecDims[15];
                        dims[16] = Box_GirderSecDims[16];
                        dims[17] = Box_GirderSecDims[17];
                        dims[18] = Box_GirderSecDims[18];
                        dims[19] = Box_GirderSecDims[19];
                        double[,] XY;
                        XY = STemp.SecPointsByShape("Box_Girder", dims, false, null);
                        m_View.AddPolygon(XY, 1, 0);
                        m_View.ZoomFit();
                        pbSection.Image = m_View.GetPicture();
                    }
                    catch
                    {
                        MessageBox.Show("Input value was not in a correct format.", "Error");
                    }
                    break;
                case "U_Girder":
                    int iU = getRow;
                    double[] U_GirderSecDims = m_SectionList[iU].getShapeDims();
                    m_View = new Kanvas2D();
                    m_View.SetWinSize(pbSection.Size.Width, pbSection.Size.Height);
                    m_View.AddPen(new Pen(Color.Green));
                    try
                    {
                        double[] dims = new double[9];
                        dims[0] = U_GirderSecDims[0];
                        dims[1] = U_GirderSecDims[1];
                        dims[2] = U_GirderSecDims[2];
                        dims[3] = U_GirderSecDims[3];
                        dims[4] = U_GirderSecDims[4];
                        dims[5] = U_GirderSecDims[5];
                        dims[6] = U_GirderSecDims[6];
                        dims[7] = U_GirderSecDims[7];
                        dims[8] = U_GirderSecDims[8];
                        double[,] XY;
                        XY = STemp.SecPointsByShape("U_Girder", dims, false, null);
                        m_View.AddPolygon(XY, 1, 0);
                        m_View.ZoomFit();
                        pbSection.Image = m_View.GetPicture();
                    }
                    catch
                    {
                        MessageBox.Show("Input value was not in a correct format.", "Error");
                    }
                    break;
                case "PC_Box":
                    int iP = getRow;
                    double[] PC_BoxSecDims = m_SectionList[iP].getShapeDims();
                    m_View = new Kanvas2D();
                    m_View.SetWinSize(pbSection.Size.Width, pbSection.Size.Height);
                    m_View.AddPen(new Pen(Color.Green));
                    try
                    {
                        double[] dims = new double[6];
                        dims[0] = PC_BoxSecDims[0];
                        dims[1] = PC_BoxSecDims[1];
                        dims[2] = PC_BoxSecDims[2];
                        dims[3] = PC_BoxSecDims[3];
                        dims[4] = PC_BoxSecDims[4];
                        dims[5] = PC_BoxSecDims[5];
                        double[,] XY;
                        XY = STemp.SecPointsByShape("PC_Box", dims, false, null);
                        m_View.AddPolygon(XY, 1, 0);
                        m_View.ZoomFit();
                        pbSection.Image = m_View.GetPicture();
                    }
                    catch
                    {
                        MessageBox.Show("Input value was not in a correct format.", "Error");
                    }
                    break;
            }
        }
        
        private void frmSection_Resize(object sender, EventArgs e)
        {
            Draw();
        }

        private void frmSection_Load(object sender, EventArgs e)
        {
            Draw();
        }

    }
}
