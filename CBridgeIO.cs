﻿using System;
using System.Linq;
using ZoconFEA3D;      // Add for anything that needs to access m_Structure or core functions
using System.Data;
using System.Xml;
using System.Globalization;

namespace CBridge
{
    class CBridgeIO
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        public void fillInputLiraryFromXML(InputLibrary m_InputLibrary, string FileName)
        {

            // read Materials
            XmlDocument doc = new XmlDocument();
            doc.Load(FileName);

            XmlNodeList elemList = doc.GetElementsByTagName("Material");

            if (elemList.Count > 0)
            {
                fillMaterials(ref m_InputLibrary.Materials, elemList);
            }

            elemList = doc.GetElementsByTagName("Alignment");

            if (elemList.Count > 0)
            {
                fillAlignments(ref m_InputLibrary.Alignments, elemList);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillMaterials(ref Material[] Materials, XmlNodeList elemList)
        { 
            // update input library Materials from xml file
            Materials = new Material[elemList.Count];

            string MatType = "";
            string MatName = "";
            double[] Prop = new double[6];
            int i = 0;

            foreach (XmlNode node in elemList)
            {
                MatType = node.Attributes["type"].Value;
                MatName = node.InnerText;

                Prop[0] = Convert.ToDouble(node.Attributes["E"].Value);
                Prop[1] = Convert.ToDouble(node.Attributes["UnitWeight"].Value);
                Prop[2] = Convert.ToDouble(node.Attributes["PR"].Value);

                if (MatType == "Concrete")
                {
                    Prop[3] = Convert.ToDouble(node.Attributes["fc"].Value);
                    Prop[4] = Convert.ToDouble(node.Attributes["fci"].Value);
                }
                else if ((MatType == "Mild") || (MatType == "Strand"))
                {
                    Prop[3] = Convert.ToDouble(node.Attributes["fy"].Value);
                    Prop[4] = Convert.ToDouble(node.Attributes["fu"].Value);
                    Prop[5] = Convert.ToDouble(node.Attributes["Grade"].Value);
                }

                //////////////////////////////////////////////////////////
                //
                // update input Library
                //
                //////////////////////////////////////////////////////////
                if (MatType == "Elastic")
                {
                    Material elastic = new Material(Prop[0], Prop[1], Prop[2], MatName);
                    elastic.DisplayName = MatName;

                    Materials[i++] = elastic;

                }
                else if (MatType == "Concrete")
                {
                    MaterialConc concrete = new MaterialConc(Prop[3], Prop[4], Prop[0], Prop[1], Prop[2], MatName);
                    concrete.DisplayName = MatName;

                    Materials[i++] = concrete;

                }
                else if (MatType == "Mild")
                {
                    MaterialSteel mildSteel = new MaterialSteel("Mild", Prop[3], Prop[4], Prop[0], Prop[1], Prop[2], MatName);
                    mildSteel.DisplayName = MatName;

                    Materials[i++] = mildSteel;

                }
                else if (MatType == "Strand")
                {
                    MaterialSteel strandSteel = new MaterialSteel("Strand", Prop[3], Prop[4], Prop[0], Prop[1], Prop[2], MatName);
                    strandSteel.DisplayName = MatName;

                    Materials[i++] = strandSteel;
                }
            }
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void fillAlignments(ref Alignment[] Alignment, XmlNodeList elemList)
        {
            
            // update input library Alignments from xml file
            Alignment = new Alignment[1];

            string Name = "";
            double[] XYZStart = new double[3];
            double AzStart = 0;
            double VSlopeStart = 0;
            string Type = "";

            int ih = 0;
            int iv = 0;
            
            // count the number of horizontal and vertical rows in XML
            foreach (XmlNode node in elemList)
            {
                Type = node.Attributes["type"].Value;

                if (Type == "Horizontal")
                {
                    ih++;
                }
                else if (Type == "Vertical")
                {
                    iv++;
                }
            }
           
            double[] CrvRad    = new double[ih];
            double[] CrvLen    = new double[ih];
                      
            double[] VCrvRun   = new double[iv];
            double[] VCrvRise  = new double[iv];

            // now read XML nodes
            ih = iv = 0;

            foreach (XmlNode node in elemList)
            {
                Type = node.Attributes["type"].Value;

                if (Type == "Start")
                {
                    XYZStart[0] = Convert.ToDouble(node.Attributes["StartX"].Value);
                    XYZStart[1] = Convert.ToDouble(node.Attributes["StartY"].Value);
                    XYZStart[2] = Convert.ToDouble(node.Attributes["StartZ"].Value);

                    AzStart     = Convert.ToDouble(node.Attributes["AzStart"].Value);
                    VSlopeStart = Convert.ToDouble(node.Attributes["VSlopeStart"].Value);

                }
                else if (Type == "Horizontal")
                {
                    CrvRad[ih] = Convert.ToDouble(node.Attributes["Radius"].Value);
                    CrvLen[ih] = Convert.ToDouble(node.Attributes["Length"].Value);

                    ih++;

                }
                else if (Type == "Vertical")
                {
                    VCrvRun[iv]  = Convert.ToDouble(node.Attributes["Rise"].Value);
                    VCrvRise[iv] = Convert.ToDouble(node.Attributes["Run"].Value);

                    iv++;
                }
            }

            Alignment alignment = new Alignment(Name, XYZStart, AzStart, CrvRad, CrvLen);

            alignment.SetVerticalCurve(VSlopeStart, VCrvRun, VCrvRise);

            Alignment[0] = alignment;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        public void saveInputLiraryToXML(InputLibrary m_InputLibrary, string FileName)
        {

            XmlWriter writer = null;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            DateTime localDate = DateTime.Now;

            writer = XmlWriter.Create(FileName, settings);
            writer.WriteComment("Savind CBridge data from  : " + localDate.ToString());

            // Write the root element
            writer.WriteStartElement("BridgeInput");
            
            // add material to XML 
            int materialCount = m_InputLibrary.Materials.Count();

            if (m_InputLibrary.Materials.Count() > 0)
            {
                saveMaterialsToXML(m_InputLibrary.Materials, writer);
            }

            if (m_InputLibrary.Alignments.Count() > 0)
            {
                saveAlignmentsToXML(m_InputLibrary.Alignments, writer);
            }

            writer.WriteEndDocument();

            writer.Flush();
            writer.Close();
   
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveMaterialsToXML(Material[] Materials, XmlWriter writer)
        {
         
            // Write Material elements 
            writer.WriteStartElement("Materials");

            foreach (Material item in Materials)
            {
                writer.WriteStartElement("Material");
              
                writer.WriteAttributeString("E", item.E.ToString());
                writer.WriteAttributeString("UnitWeight", item.gamma.ToString());
                writer.WriteAttributeString("PR", item.v.ToString());

                if (item.GetType() == typeof(MaterialConc))
                {
                    writer.WriteAttributeString("fc", ((MaterialConc)item).fc.ToString());
                    writer.WriteAttributeString("fci", ((MaterialConc)item).fci.ToString());
                    writer.WriteAttributeString("type", "Concrete");

                }
                else if (item.GetType() == typeof(MaterialSteel))
                {
                    writer.WriteAttributeString("fy", ((MaterialSteel)item).fy.ToString());
                    writer.WriteAttributeString("fu", ((MaterialSteel)item).fu.ToString());
                    writer.WriteAttributeString("Grade", ((MaterialSteel)item).E.ToString());
                    writer.WriteAttributeString("type", ((MaterialSteel)item).type);
                }
                else
                {
                    writer.WriteAttributeString("type", "Elastic");
                }
                
                writer.WriteString(item.MaterialName);
                writer.WriteEndElement();
            }

            // end "Materials"
            writer.WriteEndElement();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void saveAlignmentsToXML(Alignment[] Alignments, XmlWriter writer)
        {

            Alignment item = Alignments[0];

            // Write an element (this one is the root).
            writer.WriteStartElement("Alignments");

            writer.WriteStartElement("Alignment");
            writer.WriteAttributeString("StartX", item.XYZ0[0].ToString());
            writer.WriteAttributeString("StartY", item.XYZ0[1].ToString());
            writer.WriteAttributeString("StartZ", item.XYZ0[2].ToString());
            writer.WriteAttributeString("AzStart", item.CrvAzStart[0].ToString());
            writer.WriteAttributeString("VSlopeStart", item.m_VSlopeStart.ToString());  // must use get set
            writer.WriteAttributeString("type", "Start");
            writer.WriteEndElement();

            // do horizontal Curves, one lone per
            for (int i = 0; i < item.CrvRadius.Length; i++)
            {
                writer.WriteStartElement("Alignment");
                writer.WriteAttributeString("Radius", item.CrvRadius[i].ToString());
                writer.WriteAttributeString("Length", item.CrvLength[i].ToString());
                writer.WriteAttributeString("type", "Horizontal");
                writer.WriteEndElement();
            }

            // do horizontal Curves, one lone per
            for (int i = 0; i < item.m_VCrvRun.Length; i++)
            {
                writer.WriteStartElement("Alignment");
                writer.WriteAttributeString("Rise", item.m_VCrvRise[i].ToString());
                writer.WriteAttributeString("Run", item.m_VCrvRun[i].ToString());
                writer.WriteAttributeString("type", "Vertical");
                writer.WriteEndElement();
            }

            // end "Alignments"
            writer.WriteEndElement();
        }

    }
}     