﻿namespace CBridge
{
    partial class frmMaterials
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvMaterialMildSteel = new System.Windows.Forms.DataGridView();
            this.dgvMMS_InName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_E = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_PR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_UW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_FY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_FU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMMS_Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bReplicate = new System.Windows.Forms.Button();
            this.dgvMaterialElastic = new System.Windows.Forms.DataGridView();
            this.dgvME_InName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvME_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvME_InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvME_E = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvME_PR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvME_UW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMaterialConcrete = new System.Windows.Forms.DataGridView();
            this.dgvMC_InName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC_InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC_E = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC_PR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC_UW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC_FC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMC_FCI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMaterialStrand = new System.Windows.Forms.DataGridView();
            this.dgvMS_InName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_E = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_PR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_UW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_FY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_FU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMS_Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.tabMaterial = new System.Windows.Forms.TabControl();
            this.Concrete = new System.Windows.Forms.TabPage();
            this.MildSteel = new System.Windows.Forms.TabPage();
            this.Strand = new System.Windows.Forms.TabPage();
            this.Elastic = new System.Windows.Forms.TabPage();
            this.lMateraial = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialMildSteel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialElastic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialConcrete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialStrand)).BeginInit();
            this.tabMaterial.SuspendLayout();
            this.Concrete.SuspendLayout();
            this.MildSteel.SuspendLayout();
            this.Strand.SuspendLayout();
            this.Elastic.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMaterialMildSteel
            // 
            this.dgvMaterialMildSteel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMaterialMildSteel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMaterialMildSteel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialMildSteel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvMMS_InName,
            this.dgvMMS_Name,
            this.dgvMMS_InUse,
            this.dgvMMS_E,
            this.dgvMMS_PR,
            this.dgvMMS_UW,
            this.dgvMMS_FY,
            this.dgvMMS_FU,
            this.dgvMMS_Grade});
            this.dgvMaterialMildSteel.Location = new System.Drawing.Point(0, 0);
            this.dgvMaterialMildSteel.Margin = new System.Windows.Forms.Padding(2);
            this.dgvMaterialMildSteel.Name = "dgvMaterialMildSteel";
            this.dgvMaterialMildSteel.RowTemplate.Height = 24;
            this.dgvMaterialMildSteel.Size = new System.Drawing.Size(984, 277);
            this.dgvMaterialMildSteel.TabIndex = 26;
            this.dgvMaterialMildSteel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialMildSteel_CellValueChanged);
            this.dgvMaterialMildSteel.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMaterialMildSteel_RowsAdded);
            this.dgvMaterialMildSteel.SelectionChanged += new System.EventHandler(this.dgvMaterialMildSteel_SelectionChanged);
            // 
            // dgvMMS_InName
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Blue;
            this.dgvMMS_InName.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMMS_InName.HeaderText = "InName";
            this.dgvMMS_InName.Name = "dgvMMS_InName";
            // 
            // dgvMMS_Name
            // 
            this.dgvMMS_Name.HeaderText = "Name";
            this.dgvMMS_Name.Name = "dgvMMS_Name";
            // 
            // dgvMMS_InUse
            // 
            this.dgvMMS_InUse.HeaderText = "Used";
            this.dgvMMS_InUse.Name = "dgvMMS_InUse";
            // 
            // dgvMMS_E
            // 
            this.dgvMMS_E.HeaderText = "E (ksi)";
            this.dgvMMS_E.Name = "dgvMMS_E";
            // 
            // dgvMMS_PR
            // 
            this.dgvMMS_PR.HeaderText = "Piosson Ratio";
            this.dgvMMS_PR.Name = "dgvMMS_PR";
            // 
            // dgvMMS_UW
            // 
            this.dgvMMS_UW.HeaderText = "U.W. (ksi)";
            this.dgvMMS_UW.Name = "dgvMMS_UW";
            // 
            // dgvMMS_FY
            // 
            this.dgvMMS_FY.HeaderText = "fy (ksi)";
            this.dgvMMS_FY.Name = "dgvMMS_FY";
            // 
            // dgvMMS_FU
            // 
            this.dgvMMS_FU.HeaderText = "fu (ksi)";
            this.dgvMMS_FU.Name = "dgvMMS_FU";
            // 
            // dgvMMS_Grade
            // 
            this.dgvMMS_Grade.HeaderText = "Grade";
            this.dgvMMS_Grade.Name = "dgvMMS_Grade";
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCancel.ForeColor = System.Drawing.Color.DarkRed;
            this.bCancel.Location = new System.Drawing.Point(933, 345);
            this.bCancel.Margin = new System.Windows.Forms.Padding(2);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(73, 28);
            this.bCancel.TabIndex = 35;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bOK.ForeColor = System.Drawing.Color.DarkRed;
            this.bOK.Location = new System.Drawing.Point(856, 345);
            this.bOK.Margin = new System.Windows.Forms.Padding(2);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(73, 28);
            this.bOK.TabIndex = 34;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bDelete.ForeColor = System.Drawing.Color.DarkRed;
            this.bDelete.Location = new System.Drawing.Point(90, 345);
            this.bDelete.Margin = new System.Windows.Forms.Padding(2);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(73, 28);
            this.bDelete.TabIndex = 33;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bReplicate
            // 
            this.bReplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bReplicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReplicate.ForeColor = System.Drawing.Color.DarkRed;
            this.bReplicate.Location = new System.Drawing.Point(13, 345);
            this.bReplicate.Margin = new System.Windows.Forms.Padding(2);
            this.bReplicate.Name = "bReplicate";
            this.bReplicate.Size = new System.Drawing.Size(73, 28);
            this.bReplicate.TabIndex = 32;
            this.bReplicate.TabStop = false;
            this.bReplicate.Text = "Replicate";
            this.bReplicate.UseVisualStyleBackColor = true;
            this.bReplicate.Click += new System.EventHandler(this.bReplicate_Click);
            // 
            // dgvMaterialElastic
            // 
            this.dgvMaterialElastic.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMaterialElastic.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMaterialElastic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialElastic.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvME_InName,
            this.dgvME_Name,
            this.dgvME_InUse,
            this.dgvME_E,
            this.dgvME_PR,
            this.dgvME_UW});
            this.dgvMaterialElastic.Location = new System.Drawing.Point(2, 2);
            this.dgvMaterialElastic.Margin = new System.Windows.Forms.Padding(2);
            this.dgvMaterialElastic.Name = "dgvMaterialElastic";
            this.dgvMaterialElastic.RowTemplate.Height = 24;
            this.dgvMaterialElastic.Size = new System.Drawing.Size(982, 275);
            this.dgvMaterialElastic.TabIndex = 30;
            this.dgvMaterialElastic.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialElastic_CellContentClick);
            this.dgvMaterialElastic.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialElastic_CellValueChanged);
            this.dgvMaterialElastic.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMaterialElastic_RowsAdded);
            this.dgvMaterialElastic.SelectionChanged += new System.EventHandler(this.dgvMaterialElastic_SelectionChanged);
            // 
            // dgvME_InName
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            this.dgvME_InName.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvME_InName.HeaderText = "InName";
            this.dgvME_InName.Name = "dgvME_InName";
            // 
            // dgvME_Name
            // 
            this.dgvME_Name.HeaderText = "Name";
            this.dgvME_Name.Name = "dgvME_Name";
            // 
            // dgvME_InUse
            // 
            this.dgvME_InUse.HeaderText = "Used";
            this.dgvME_InUse.Name = "dgvME_InUse";
            // 
            // dgvME_E
            // 
            this.dgvME_E.HeaderText = "E (ksi)";
            this.dgvME_E.Name = "dgvME_E";
            // 
            // dgvME_PR
            // 
            this.dgvME_PR.HeaderText = "Piosson Ratio";
            this.dgvME_PR.Name = "dgvME_PR";
            // 
            // dgvME_UW
            // 
            this.dgvME_UW.HeaderText = "U.W. (ksi)";
            this.dgvME_UW.Name = "dgvME_UW";
            // 
            // dgvMaterialConcrete
            // 
            this.dgvMaterialConcrete.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMaterialConcrete.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMaterialConcrete.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dgvMaterialConcrete.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMaterialConcrete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMaterialConcrete.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialConcrete.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvMC_InName,
            this.dgvMC_Name,
            this.dgvMC_InUse,
            this.dgvMC_E,
            this.dgvMC_PR,
            this.dgvMC_UW,
            this.dgvMC_FC,
            this.dgvMC_FCI});
            this.dgvMaterialConcrete.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgvMaterialConcrete.Location = new System.Drawing.Point(4, 4);
            this.dgvMaterialConcrete.Margin = new System.Windows.Forms.Padding(4);
            this.dgvMaterialConcrete.Name = "dgvMaterialConcrete";
            this.dgvMaterialConcrete.RowTemplate.Height = 24;
            this.dgvMaterialConcrete.Size = new System.Drawing.Size(986, 271);
            this.dgvMaterialConcrete.TabIndex = 24;
            this.dgvMaterialConcrete.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialConcrete_CellValueChanged);
            this.dgvMaterialConcrete.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMaterialConcrete_RowsAdded);
            this.dgvMaterialConcrete.SelectionChanged += new System.EventHandler(this.dgvMaterialConcrete_SelectionChanged);
            // 
            // dgvMC_InName
            // 
            this.dgvMC_InName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvMC_InName.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMC_InName.Frozen = true;
            this.dgvMC_InName.HeaderText = "InName";
            this.dgvMC_InName.Name = "dgvMC_InName";
            this.dgvMC_InName.ReadOnly = true;
            this.dgvMC_InName.Width = 69;
            // 
            // dgvMC_Name
            // 
            this.dgvMC_Name.HeaderText = "Name";
            this.dgvMC_Name.Name = "dgvMC_Name";
            // 
            // dgvMC_InUse
            // 
            this.dgvMC_InUse.FillWeight = 50F;
            this.dgvMC_InUse.HeaderText = "Used";
            this.dgvMC_InUse.Name = "dgvMC_InUse";
            // 
            // dgvMC_E
            // 
            this.dgvMC_E.HeaderText = "E (ksi)";
            this.dgvMC_E.Name = "dgvMC_E";
            // 
            // dgvMC_PR
            // 
            this.dgvMC_PR.FillWeight = 110F;
            this.dgvMC_PR.HeaderText = "Piosson Ratio";
            this.dgvMC_PR.Name = "dgvMC_PR";
            // 
            // dgvMC_UW
            // 
            this.dgvMC_UW.HeaderText = "U. W (ksi)";
            this.dgvMC_UW.Name = "dgvMC_UW";
            // 
            // dgvMC_FC
            // 
            this.dgvMC_FC.HeaderText = "f\'c (ksi)";
            this.dgvMC_FC.Name = "dgvMC_FC";
            // 
            // dgvMC_FCI
            // 
            this.dgvMC_FCI.HeaderText = "f\'ci (ksi)";
            this.dgvMC_FCI.Name = "dgvMC_FCI";
            // 
            // dgvMaterialStrand
            // 
            this.dgvMaterialStrand.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMaterialStrand.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMaterialStrand.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaterialStrand.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvMS_InName,
            this.dgvMS_Name,
            this.dgvMS_InUse,
            this.dgvMS_E,
            this.dgvMS_PR,
            this.dgvMS_UW,
            this.dgvMS_FY,
            this.dgvMS_FU,
            this.dgvMS_Grade});
            this.dgvMaterialStrand.Location = new System.Drawing.Point(0, 2);
            this.dgvMaterialStrand.Margin = new System.Windows.Forms.Padding(2);
            this.dgvMaterialStrand.Name = "dgvMaterialStrand";
            this.dgvMaterialStrand.RowTemplate.Height = 24;
            this.dgvMaterialStrand.Size = new System.Drawing.Size(984, 275);
            this.dgvMaterialStrand.TabIndex = 28;
            this.dgvMaterialStrand.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialStrand_CellContentClick);
            this.dgvMaterialStrand.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMaterialStrand_CellValueChanged);
            this.dgvMaterialStrand.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvMaterialStrand_RowsAdded);
            this.dgvMaterialStrand.SelectionChanged += new System.EventHandler(this.dgvMaterialStrand_SelectionChanged);
            // 
            // dgvMS_InName
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Blue;
            this.dgvMS_InName.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvMS_InName.HeaderText = "InName";
            this.dgvMS_InName.Name = "dgvMS_InName";
            // 
            // dgvMS_Name
            // 
            this.dgvMS_Name.HeaderText = "Name";
            this.dgvMS_Name.Name = "dgvMS_Name";
            // 
            // dgvMS_InUse
            // 
            this.dgvMS_InUse.HeaderText = "Used";
            this.dgvMS_InUse.Name = "dgvMS_InUse";
            // 
            // dgvMS_E
            // 
            this.dgvMS_E.HeaderText = "E (ksi)";
            this.dgvMS_E.Name = "dgvMS_E";
            // 
            // dgvMS_PR
            // 
            this.dgvMS_PR.HeaderText = "Piosson Ratio";
            this.dgvMS_PR.Name = "dgvMS_PR";
            // 
            // dgvMS_UW
            // 
            this.dgvMS_UW.HeaderText = "U.W. (kci)";
            this.dgvMS_UW.Name = "dgvMS_UW";
            // 
            // dgvMS_FY
            // 
            this.dgvMS_FY.HeaderText = "fy (ksi)";
            this.dgvMS_FY.Name = "dgvMS_FY";
            // 
            // dgvMS_FU
            // 
            this.dgvMS_FU.HeaderText = "fu (ksi)";
            this.dgvMS_FU.Name = "dgvMS_FU";
            // 
            // dgvMS_Grade
            // 
            this.dgvMS_Grade.HeaderText = "Grade";
            this.dgvMS_Grade.Name = "dgvMS_Grade";
            // 
            // cmbUnit
            // 
            this.cmbUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnit.ForeColor = System.Drawing.Color.DarkRed;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Items.AddRange(new object[] {
            "lb, in",
            "lb, ft",
            "kip, in",
            "kip, ft"});
            this.cmbUnit.Location = new System.Drawing.Point(782, 350);
            this.cmbUnit.Margin = new System.Windows.Forms.Padding(2);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(70, 21);
            this.cmbUnit.TabIndex = 36;
            this.cmbUnit.Text = "kip, in";
            this.cmbUnit.SelectedIndexChanged += new System.EventHandler(this.cmbUnit_SelectedIndexChanged);
            // 
            // tabMaterial
            // 
            this.tabMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMaterial.Controls.Add(this.Concrete);
            this.tabMaterial.Controls.Add(this.MildSteel);
            this.tabMaterial.Controls.Add(this.Strand);
            this.tabMaterial.Controls.Add(this.Elastic);
            this.tabMaterial.Location = new System.Drawing.Point(12, 35);
            this.tabMaterial.Name = "tabMaterial";
            this.tabMaterial.SelectedIndex = 0;
            this.tabMaterial.Size = new System.Drawing.Size(994, 305);
            this.tabMaterial.TabIndex = 37;
            // 
            // Concrete
            // 
            this.Concrete.Controls.Add(this.dgvMaterialConcrete);
            this.Concrete.Location = new System.Drawing.Point(4, 22);
            this.Concrete.Name = "Concrete";
            this.Concrete.Padding = new System.Windows.Forms.Padding(3);
            this.Concrete.Size = new System.Drawing.Size(986, 279);
            this.Concrete.TabIndex = 0;
            this.Concrete.Text = "Concrete";
            this.Concrete.UseVisualStyleBackColor = true;
            // 
            // MildSteel
            // 
            this.MildSteel.Controls.Add(this.dgvMaterialMildSteel);
            this.MildSteel.Location = new System.Drawing.Point(4, 22);
            this.MildSteel.Name = "MildSteel";
            this.MildSteel.Padding = new System.Windows.Forms.Padding(3);
            this.MildSteel.Size = new System.Drawing.Size(986, 279);
            this.MildSteel.TabIndex = 1;
            this.MildSteel.Text = "MildSteel";
            this.MildSteel.UseVisualStyleBackColor = true;
            // 
            // Strand
            // 
            this.Strand.Controls.Add(this.dgvMaterialStrand);
            this.Strand.Location = new System.Drawing.Point(4, 22);
            this.Strand.Name = "Strand";
            this.Strand.Size = new System.Drawing.Size(986, 279);
            this.Strand.TabIndex = 2;
            this.Strand.Text = "Strand";
            this.Strand.UseVisualStyleBackColor = true;
            // 
            // Elastic
            // 
            this.Elastic.Controls.Add(this.dgvMaterialElastic);
            this.Elastic.Location = new System.Drawing.Point(4, 22);
            this.Elastic.Name = "Elastic";
            this.Elastic.Size = new System.Drawing.Size(986, 279);
            this.Elastic.TabIndex = 3;
            this.Elastic.Text = "Elastic";
            this.Elastic.UseVisualStyleBackColor = true;
            // 
            // lMateraial
            // 
            this.lMateraial.AutoSize = true;
            this.lMateraial.Location = new System.Drawing.Point(18, 13);
            this.lMateraial.Name = "lMateraial";
            this.lMateraial.Size = new System.Drawing.Size(97, 13);
            this.lMateraial.TabIndex = 38;
            this.lMateraial.Text = "Material Properties:";
            // 
            // frmMaterials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 384);
            this.Controls.Add(this.lMateraial);
            this.Controls.Add(this.tabMaterial);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.bReplicate);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmMaterials";
            this.Text = "FrmMaterials";
            this.Load += new System.EventHandler(this.frmMaterials_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialMildSteel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialElastic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialConcrete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaterialStrand)).EndInit();
            this.tabMaterial.ResumeLayout(false);
            this.Concrete.ResumeLayout(false);
            this.MildSteel.ResumeLayout(false);
            this.Strand.ResumeLayout(false);
            this.Elastic.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bReplicate;


        private System.Windows.Forms.DataGridView dgvMaterialConcrete;

        private System.Windows.Forms.DataGridView dgvMaterialMildSteel;

        private System.Windows.Forms.DataGridView dgvMaterialStrand;

        private System.Windows.Forms.DataGridView dgvMaterialElastic;


        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.TabControl tabMaterial;
        private System.Windows.Forms.TabPage Concrete;
        private System.Windows.Forms.TabPage MildSteel;
        private System.Windows.Forms.TabPage Strand;
        private System.Windows.Forms.TabPage Elastic;
        private System.Windows.Forms.Label lMateraial;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_InName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_InUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_E;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_PR;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_UW;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_FC;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMC_FCI;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_InName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_InUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_E;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_PR;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_UW;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_FY;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_FU;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMMS_Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_InName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_InUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_E;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_PR;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_UW;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_FY;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_FU;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvMS_Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvME_InName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvME_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvME_InUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvME_E;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvME_PR;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvME_UW;
    }
}