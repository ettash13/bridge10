﻿namespace CBridge
{
    partial class frmSection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSection = new System.Windows.Forms.DataGridView();
            this.lSections = new System.Windows.Forms.Label();
            this.pbSection = new System.Windows.Forms.PictureBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.splitSection = new System.Windows.Forms.SplitContainer();
            this.bDelete = new System.Windows.Forms.Button();
            this.bReplicate = new System.Windows.Forms.Button();
            this.InternalName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InUse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modify = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Properties = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitSection)).BeginInit();
            this.splitSection.Panel1.SuspendLayout();
            this.splitSection.Panel2.SuspendLayout();
            this.splitSection.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvSection
            // 
            this.dgvSection.AllowUserToDeleteRows = false;
            this.dgvSection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSection.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSection.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InternalName,
            this.DisplayName,
            this.InUse,
            this.Type,
            this.Modify,
            this.Properties});
            this.dgvSection.Location = new System.Drawing.Point(3, 3);
            this.dgvSection.Name = "dgvSection";
            this.dgvSection.Size = new System.Drawing.Size(654, 181);
            this.dgvSection.TabIndex = 0;
            this.dgvSection.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSection_CellClick);
            this.dgvSection.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSection_CellValueChanged);
            this.dgvSection.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvSection_RowsAdded);
            this.dgvSection.SelectionChanged += new System.EventHandler(this.dgvSection_SelectionChanged);
            // 
            // lSections
            // 
            this.lSections.AutoSize = true;
            this.lSections.Location = new System.Drawing.Point(13, 13);
            this.lSections.Name = "lSections";
            this.lSections.Size = new System.Drawing.Size(77, 13);
            this.lSections.TabIndex = 1;
            this.lSections.Text = "Cross Sections";
            // 
            // pbSection
            // 
            this.pbSection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbSection.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pbSection.Location = new System.Drawing.Point(3, 3);
            this.pbSection.Name = "pbSection";
            this.pbSection.Size = new System.Drawing.Size(654, 329);
            this.pbSection.TabIndex = 2;
            this.pbSection.TabStop = false;
            // 
            // bCancel
            // 
            this.bCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bCancel.Location = new System.Drawing.Point(594, 563);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bOK.Location = new System.Drawing.Point(513, 563);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(75, 23);
            this.bOK.TabIndex = 4;
            this.bOK.Text = "OK";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // splitSection
            // 
            this.splitSection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitSection.Location = new System.Drawing.Point(12, 31);
            this.splitSection.Name = "splitSection";
            this.splitSection.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitSection.Panel1
            // 
            this.splitSection.Panel1.Controls.Add(this.dgvSection);
            // 
            // splitSection.Panel2
            // 
            this.splitSection.Panel2.Controls.Add(this.pbSection);
            this.splitSection.Size = new System.Drawing.Size(660, 526);
            this.splitSection.SplitterDistance = 187;
            this.splitSection.TabIndex = 5;
            // 
            // bDelete
            // 
            this.bDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bDelete.Location = new System.Drawing.Point(16, 563);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(75, 23);
            this.bDelete.TabIndex = 6;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bReplicate
            // 
            this.bReplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bReplicate.Location = new System.Drawing.Point(97, 563);
            this.bReplicate.Name = "bReplicate";
            this.bReplicate.Size = new System.Drawing.Size(75, 23);
            this.bReplicate.TabIndex = 7;
            this.bReplicate.Text = "Replicate";
            this.bReplicate.UseVisualStyleBackColor = true;
            this.bReplicate.Click += new System.EventHandler(this.bReplicate_Click);
            // 
            // InternalName
            // 
            this.InternalName.HeaderText = "InternalName";
            this.InternalName.Name = "InternalName";
            this.InternalName.Visible = false;
            // 
            // DisplayName
            // 
            this.DisplayName.FillWeight = 96.4467F;
            this.DisplayName.HeaderText = "Name";
            this.DisplayName.Name = "DisplayName";
            // 
            // InUse
            // 
            this.InUse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.InUse.FillWeight = 114.2132F;
            this.InUse.HeaderText = "In Use";
            this.InUse.Name = "InUse";
            this.InUse.ReadOnly = true;
            this.InUse.Width = 45;
            // 
            // Type
            // 
            this.Type.FillWeight = 96.4467F;
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // Modify
            // 
            this.Modify.FillWeight = 96.4467F;
            this.Modify.HeaderText = "Modify";
            this.Modify.Name = "Modify";
            this.Modify.Text = "Modify";
            this.Modify.UseColumnTextForButtonValue = true;
            // 
            // Properties
            // 
            this.Properties.FillWeight = 96.4467F;
            this.Properties.HeaderText = "Properties";
            this.Properties.Name = "Properties";
            this.Properties.Text = "Properties";
            this.Properties.UseColumnTextForButtonValue = true;
            // 
            // frmSection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 608);
            this.Controls.Add(this.bReplicate);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.splitSection);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.lSections);
            this.Name = "frmSection";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Section";
            this.Load += new System.EventHandler(this.frmSection_Load);
            this.Resize += new System.EventHandler(this.frmSection_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSection)).EndInit();
            this.splitSection.Panel1.ResumeLayout(false);
            this.splitSection.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitSection)).EndInit();
            this.splitSection.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvSection;
        private System.Windows.Forms.Label lSections;
        private System.Windows.Forms.PictureBox pbSection;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.SplitContainer splitSection;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bReplicate;
        private System.Windows.Forms.DataGridViewTextBoxColumn InternalName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn InUse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewButtonColumn Modify;
        private System.Windows.Forms.DataGridViewButtonColumn Properties;
    }
}