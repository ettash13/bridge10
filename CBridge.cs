﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ZoconFEA3D;      // Add for anything that needs to access m_Structure or core functions
using System.Xml;
using System.IO;
using System.Windows.Forms;

namespace CBridge
{
    //     
    /// <summary>
    /// Top Level Design
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //    
    public partial class CBridge : Form
    {
        public InputLibrary m_InputLibrary;
        
        // Graphic
        Bitmap m_DrawArea;
        Graphics m_gModel;

        Kanvas3D m_MainView;

        String m_ModelFilename = "";

        public CBridge()
        {
            InitializeComponent();
            m_InputLibrary = new InputLibrary();
            
            m_DrawArea = new Bitmap(pbModel.Size.Width, pbModel.Size.Height);
            m_gModel = Graphics.FromImage(m_DrawArea);
            Graphics g = m_gModel;
            m_MainView = new Kanvas3D();
            Pen Redpen = new Pen(Color.Red);
            Pen Bluepen = new Pen(Color.Blue);
            m_MainView.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_MainView.AddPen(Redpen); //1
            m_MainView.AddPen(Bluepen); //2
            DrawLine();
        }
        
   
        // --------------------------------------------------------------------
        // Graphic Functions
        // --------------------------------------------------------------------
              
        private void DrawLine()
        {
            Bitmap DrawArea;
            Graphics g;

            DrawArea = new Bitmap(pbModel.Size.Width, pbModel.Size.Height);
            g = Graphics.FromImage(DrawArea);

            Pen mypen = new Pen(Color.Red);
            g.DrawLine(mypen, 0, 0, 200, 150);
            Pen mypenG = new Pen(Color.Green, 4);

            g.DrawLine(mypenG, 200, 150, 200, 250);
            g.DrawString("This is a diagonal line drawn on the control", new Font("Courier", 18), System.Drawing.Brushes.Black, new Point(30, 30));

            Brush Mybrush = Brushes.Red;

            g.DrawString("Second text", new Font("Courier", 8), Mybrush, new Point(30, 50));
            g.DrawLine(mypenG, 30, 50, 200, 50);

            pbModel.Image = DrawArea;

            g.Dispose();
        }

        //     
        /// <summary>
        /// Top Level Design
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //    
        private void openBridgeModel_Click(object sender, EventArgs e)
        {
            OpenFileDialog theDialog = new OpenFileDialog();
            CBridgeIO cBbridgeIO = new CBridgeIO();

            theDialog.Filter = "xml files (*.xml)|*.xml";
            theDialog.FilterIndex = 2;

            theDialog.FileName = "";

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                string FileName = theDialog.FileName;

                // Save in global name
                m_ModelFilename = FileName;

                cBbridgeIO.fillInputLiraryFromXML(m_InputLibrary, FileName);
            }
        }

        private void saveCBridgeModel_Click(object sender, EventArgs e)
        {

            if (m_ModelFilename != null)
            {
                CBridgeIO cBbridgeIO = new CBridgeIO();
                cBbridgeIO.saveInputLiraryToXML(m_InputLibrary, m_ModelFilename);
            }

            MessageBox.Show("Model Saved...");
        }


        //     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //    
        private void saveModelAs_Click(object sender, EventArgs e)
        {
            
            CBridgeIO cBridgeIO = new CBridgeIO();
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                  
            string FileName = saveFileDialog1.FileName;
            
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileName = saveFileDialog1.FileName;
                cBridgeIO.saveInputLiraryToXML(m_InputLibrary, FileName);
            }
        }
                
        //
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void exitCBridgeMenu_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Exit without Saving? ", "Some Title", MessageBoxButtons.YesNo);
            
            if (dialogResult == DialogResult.Yes)
            {
                this.Close();
            }
            else if (dialogResult == DialogResult.No)
            {
                return;
            }
        }
        
        //
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void materialMenu_Click(object sender, EventArgs e)
        {
            frmMaterials fMaterial = new frmMaterials();

            fMaterial.SetValues(m_InputLibrary.Materials);

            // ShowDialog()  Shows the Dialog and will not go to next line until Dialog is closed with this.close              
            fMaterial.ShowDialog();

            if (fMaterial.m_OK)
            {
                fMaterial.UpdateBridgeInfo(ref m_InputLibrary.Materials);
            }
        }

        //     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //
        private void bAlignment_Click(object sender, EventArgs e)
        {
            frmAlignment fAlignment = new frmAlignment();

            fAlignment.SetValues(m_InputLibrary.Alignments);

            // ShowDialog()  Shows the Dialog and will not go to next line until Dialog is closed with this.close              
            fAlignment.ShowDialog();

            if (fAlignment.m_OK)
            {
                fAlignment.UpdateBridgeInfo(ref m_InputLibrary.Alignments);
            }
        }

        //     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //    
        private void bSection_Click(object sender, EventArgs e)
        {

            //frmBridgeSection fBridgeSection = new frmBridgeSection();
            //fBridgeSection.SetValues(m_Structure.BridgeSectionSlabOnGirderList);
            //fBridgeSection.ShowDialog();

        }

        private void crossSectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSection fSection = new frmSection();
            int nSec = 0;
            if (m_InputLibrary.CrossSections != null) nSec = m_InputLibrary.CrossSections.Length;
            bool[] InUse = new bool[nSec];
            for (int i = 0; i < nSec; i++) InUse[i] = false;
            fSection.SetSectionList(m_InputLibrary.CrossSections, InUse);
            fSection.ShowDialog();
            if (fSection.m_OK) m_InputLibrary.CrossSections = fSection.m_SectionList;
        }
    }
}
