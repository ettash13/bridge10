﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using ZDraw;
using ZoconFEA3D;

namespace CBridge
{
    public partial class frmMemberSections : Form
    {
        double recWidth, recHeight;
        double Diameter;
        double igH, TFb, TFto, TFti, igtw, BFb, BFto, BFti, BFfh, BFfv;
        double bgW, bgH, TFt, BFt, IntGrdr, twebi, webtL, webOffL, webtR, webOffR, OHL, OHLtout, OHLtin, OHR, OHRtout, OHRtin, wFilletTop, hFilletTop, wFilletBot, hFilletBot;
        double ugH, Wb, Wf, OH, Wo, to, ti, ugttop, ugtbot;
        double pbH, pbW, pbttop, pbtbot, pbtw, fillet;

        string strUnit;
        Kanvas2D m_View;
        public CrossSection STemp = new CrossSection();
        public bool m_OK = false;
        public string getType;

        public frmMemberSections()
        {
            InitializeComponent();
        }

        public void SetSection(CrossSection Sec)
        {
            STemp = Sec;
        }

        private void SaveSection()
        {
            switch (cmbSelType.SelectedIndex)
            {
                case 0:
                    SaveRectangleSection();
                    break;
                case 1:
                    SaveCircleSection();
                    break;
                case 2:
                    SaveIGirderSection();
                    break;
                case 3:
                    SaveBoxGirderSection();
                    break;
                case 4:
                    SaveUGirderSection();
                    break;
                case 5:
                    SavePrecastBoxSection();
                    break;
                default:
                    break;
            }
        }

        private void ShowSection()
        {
            switch (cmbSelType.SelectedIndex)
            {
                case 0:
                    ShowRectangleSection();
                    break;
                case 1:
                    ShowCircleSection();
                    break;
                case 2:
                    ShowIGirderSection();
                    break;
                case 3:
                    ShowBoxGirderSection();
                    break;
                case 4:
                    ShowUGirderSection();
                    break;
                case 5:
                    ShowPrecastBoxSection();
                    break;
                default:
                    break;
            }
        }

        private void SaveRectangleSection()
        {
            if (cmbUnitRectangle.Text == "ft")
            {
                recWidth = Convert.ToDouble(dgvRectangle[1, 0].Value) * 12;
                recHeight = Convert.ToDouble(dgvRectangle[1, 1].Value) * 12;
                goto End;
            }
            recWidth = Convert.ToDouble(dgvRectangle[1, 0].Value);
            recHeight = Convert.ToDouble(dgvRectangle[1, 1].Value);
        End:
            double[] ShapeDims = new double[2];
            ShapeDims[0] = recWidth;
            ShapeDims[1] = recHeight;
            STemp.SetSectionShape("Rectangle", ShapeDims, false, null, true);
        }

        private void SaveCircleSection()
        {
            double unitFactor = 1.0;
            if (cmbUnitCircle.Text == "ft") unitFactor = 12.0;
            Diameter = Convert.ToDouble(dgvCircle[1, 0].Value) * unitFactor;
            double[] ShapeDims = new double[1];
            ShapeDims[0] = Diameter;
            STemp.SetSectionShape("Circle", ShapeDims, false, null, true);
        }

        private void SaveIGirderSection()
        {
            double unitFactor = 1.0;
            if (cmbUnitIGirder.Text == "ft") unitFactor = 12.0;
            igH = Convert.ToDouble(dgvIGirder[1, 0].Value) * unitFactor;
            TFb = Convert.ToDouble(dgvIGirder[1, 1].Value) * unitFactor;
            TFto = Convert.ToDouble(dgvIGirder[1, 2].Value) * unitFactor;
            TFti = Convert.ToDouble(dgvIGirder[1, 3].Value) * unitFactor;
            igtw = Convert.ToDouble(dgvIGirder[1, 4].Value) * unitFactor;
            BFb = Convert.ToDouble(dgvIGirder[1, 5].Value) * unitFactor;
            BFto = Convert.ToDouble(dgvIGirder[1, 6].Value) * unitFactor;
            BFti = Convert.ToDouble(dgvIGirder[1, 7].Value) * unitFactor;
            BFfh = Convert.ToDouble(dgvIGirder[1, 8].Value) * unitFactor;
            BFfv = Convert.ToDouble(dgvIGirder[1, 9].Value) * unitFactor;
            double[] ShapeDims = new double[10];
            ShapeDims[0] = igH;
            ShapeDims[1] = TFb;
            ShapeDims[2] = TFto;
            ShapeDims[3] = TFti;
            ShapeDims[4] = igtw;
            ShapeDims[5] = BFb;
            ShapeDims[6] = BFto;
            ShapeDims[7] = BFti;
            ShapeDims[8] = BFfh;
            ShapeDims[9] = BFfv;
            STemp.SetSectionShape("I_Girder", ShapeDims, false, null, true);
        }

        private void SaveBoxGirderSection()
        {
            double unitFactor = 1.0;
            if (cmbUnitBoxGirder.Text == "ft") unitFactor = 12.0;
            bgW = Convert.ToDouble(dgvBoxGirder[1, 0].Value) * unitFactor;
            bgH = Convert.ToDouble(dgvBoxGirder[1, 1].Value) * unitFactor;
            TFt = Convert.ToDouble(dgvBoxGirder[1, 2].Value) * unitFactor;
            BFt = Convert.ToDouble(dgvBoxGirder[1, 3].Value) * unitFactor;
            IntGrdr = Convert.ToDouble(dgvBoxGirder[1, 4].Value);
            twebi = Convert.ToDouble(dgvBoxGirder[1, 5].Value) * unitFactor;
            webtL = Convert.ToDouble(dgvBoxGirder[1, 6].Value) * unitFactor;
            webOffL = Convert.ToDouble(dgvBoxGirder[1, 7].Value) * unitFactor;
            webtR = Convert.ToDouble(dgvBoxGirder[1, 8].Value) * unitFactor;
            webOffR = Convert.ToDouble(dgvBoxGirder[1, 9].Value) * unitFactor;
            OHL = Convert.ToDouble(dgvBoxGirder[1, 10].Value) * unitFactor;
            OHLtout = Convert.ToDouble(dgvBoxGirder[1, 11].Value) * unitFactor;
            OHLtin = Convert.ToDouble(dgvBoxGirder[1, 12].Value) * unitFactor;
            OHR = Convert.ToDouble(dgvBoxGirder[1, 13].Value) * unitFactor;
            OHRtout = Convert.ToDouble(dgvBoxGirder[1, 14].Value) * unitFactor;
            OHRtin = Convert.ToDouble(dgvBoxGirder[1, 15].Value) * unitFactor;
            wFilletTop = Convert.ToDouble(dgvBoxGirder[1, 16].Value) * unitFactor;
            hFilletTop = Convert.ToDouble(dgvBoxGirder[1, 17].Value) * unitFactor;
            wFilletBot = Convert.ToDouble(dgvBoxGirder[1, 18].Value) * unitFactor;
            hFilletBot = Convert.ToDouble(dgvBoxGirder[1, 19].Value) * unitFactor;

            double[] ShapeDims = new double[20];
            ShapeDims[0] = bgW;
            ShapeDims[1] = bgH;
            ShapeDims[2] = TFt;
            ShapeDims[3] = BFt;
            ShapeDims[4] = IntGrdr;
            ShapeDims[5] = twebi;
            ShapeDims[6] = webtL;
            ShapeDims[7] = webOffL;
            ShapeDims[8] = webtR;
            ShapeDims[9] = webOffR;
            ShapeDims[10] = OHL;
            ShapeDims[11] = OHLtout;
            ShapeDims[12] = OHLtin;
            ShapeDims[13] = OHR;
            ShapeDims[14] = OHRtout;
            ShapeDims[15] = OHRtin;
            ShapeDims[16] = wFilletTop;
            ShapeDims[17] = hFilletTop;
            ShapeDims[18] = wFilletBot;
            ShapeDims[19] = hFilletBot;
            STemp.SetSectionShape("Box_Girder", ShapeDims, false, null, true);
        }

        private void SaveUGirderSection()
        {
            double unitFactor = 1.0;
            if (cmbUnitUGirder.Text == "ft") unitFactor = 12.0;
            ugH = Convert.ToDouble(dgvUGirder[1, 0].Value) * unitFactor;
            Wb = Convert.ToDouble(dgvUGirder[1, 1].Value) * unitFactor;
            Wf = Convert.ToDouble(dgvUGirder[1, 2].Value) * unitFactor;
            OH = Convert.ToDouble(dgvUGirder[1, 3].Value) * unitFactor;
            Wo = Convert.ToDouble(dgvUGirder[1, 4].Value) * unitFactor;
            to = Convert.ToDouble(dgvUGirder[1, 5].Value) * unitFactor;
            ti = Convert.ToDouble(dgvUGirder[1, 6].Value) * unitFactor;
            ugttop = Convert.ToDouble(dgvUGirder[1, 7].Value) * unitFactor;
            ugtbot = Convert.ToDouble(dgvUGirder[1, 8].Value) * unitFactor;
            double[] ShapeDims = new double[9];
            ShapeDims[0] = ugH;
            ShapeDims[1] = Wb;
            ShapeDims[2] = Wf;
            ShapeDims[3] = OH;
            ShapeDims[4] = Wo;
            ShapeDims[5] = to;
            ShapeDims[6] = ti;
            ShapeDims[7] = ugttop;
            ShapeDims[8] = ugtbot;
            STemp.SetSectionShape("U_Girder", ShapeDims, false, null, true);
        }

        private void SavePrecastBoxSection()
        {
            double unitFactor = 1.0;
            if (cmbUnitPrecastBox.Text == "ft") unitFactor = 12.0;
            pbH = Convert.ToDouble(dgvPrecastBox[1, 0].Value) * unitFactor;
            pbW = Convert.ToDouble(dgvPrecastBox[1, 1].Value) * unitFactor;
            pbttop = Convert.ToDouble(dgvPrecastBox[1, 2].Value) * unitFactor;
            pbtbot = Convert.ToDouble(dgvPrecastBox[1, 3].Value) * unitFactor;
            pbtw = Convert.ToDouble(dgvPrecastBox[1, 4].Value) * unitFactor;
            fillet = Convert.ToDouble(dgvPrecastBox[1, 5].Value) * unitFactor;
            double[] ShapeDims = new double[6];
            ShapeDims[0] = pbH;
            ShapeDims[1] = pbW;
            ShapeDims[2] = pbttop;
            ShapeDims[3] = pbtbot;
            ShapeDims[4] = pbtw;
            ShapeDims[5] = fillet;
            STemp.SetSectionShape("PC_Box", ShapeDims, false, null, true);
        }

        private void ShowRectangleSection()
        {
            double[] SecDims = STemp.getShapeDims();
            dgvRectangle[1, 0].Value = SecDims[0];
            dgvRectangle[1, 1].Value = SecDims[1];
        }

        private void ShowCircleSection()
        {
            double[] SecDims = STemp.getShapeDims();
            dgvCircle[1, 0].Value = SecDims[0];
        }

        private void ShowIGirderSection()
        {
            double[] SecDims = STemp.getShapeDims();
            dgvIGirder[1, 0].Value = SecDims[0];
            dgvIGirder[1, 1].Value = SecDims[1];
            dgvIGirder[1, 2].Value = SecDims[2];
            dgvIGirder[1, 3].Value = SecDims[3];
            dgvIGirder[1, 4].Value = SecDims[4];
            dgvIGirder[1, 5].Value = SecDims[5];
            dgvIGirder[1, 6].Value = SecDims[6];
            dgvIGirder[1, 7].Value = SecDims[7];
            dgvIGirder[1, 8].Value = SecDims[8];
            dgvIGirder[1, 9].Value = SecDims[9];
        }

        private void ShowBoxGirderSection()
        {
            double[] SecDims = STemp.getShapeDims();
            dgvBoxGirder[1, 0].Value = SecDims[0];
            dgvBoxGirder[1, 1].Value = SecDims[1];
            dgvBoxGirder[1, 2].Value = SecDims[2];
            dgvBoxGirder[1, 3].Value = SecDims[3];
            dgvBoxGirder[1, 4].Value = SecDims[4];
            dgvBoxGirder[1, 5].Value = SecDims[5];
            dgvBoxGirder[1, 6].Value = SecDims[6];
            dgvBoxGirder[1, 7].Value = SecDims[7];
            dgvBoxGirder[1, 8].Value = SecDims[8];
            dgvBoxGirder[1, 9].Value = SecDims[9];
            dgvBoxGirder[1, 10].Value = SecDims[10];
            dgvBoxGirder[1, 11].Value = SecDims[11];
            dgvBoxGirder[1, 12].Value = SecDims[12];
            dgvBoxGirder[1, 13].Value = SecDims[13];
            dgvBoxGirder[1, 14].Value = SecDims[14];
            dgvBoxGirder[1, 15].Value = SecDims[15];
            dgvBoxGirder[1, 16].Value = SecDims[16];
            dgvBoxGirder[1, 17].Value = SecDims[17];
            dgvBoxGirder[1, 18].Value = SecDims[18];
            dgvBoxGirder[1, 19].Value = SecDims[19];
        }

        private void ShowUGirderSection()
        {
            double[] SecDims = STemp.getShapeDims();
            dgvUGirder[1, 0].Value = SecDims[0];
            dgvUGirder[1, 1].Value = SecDims[1];
            dgvUGirder[1, 2].Value = SecDims[2];
            dgvUGirder[1, 3].Value = SecDims[3];
            dgvUGirder[1, 4].Value = SecDims[4];
            dgvUGirder[1, 5].Value = SecDims[5];
            dgvUGirder[1, 6].Value = SecDims[6];
            dgvUGirder[1, 7].Value = SecDims[7];
            dgvUGirder[1, 8].Value = SecDims[8];
        }

        private void ShowPrecastBoxSection()
        {
            double[] SecDims = STemp.getShapeDims();
            dgvPrecastBox[1, 0].Value = SecDims[0];
            dgvPrecastBox[1, 1].Value = SecDims[1];
            dgvPrecastBox[1, 2].Value = SecDims[2];
            dgvPrecastBox[1, 3].Value = SecDims[3];
            dgvPrecastBox[1, 4].Value = SecDims[4];
            dgvPrecastBox[1, 5].Value = SecDims[5];
        }

        private void frmMemberSections_Load(object sender, EventArgs e)
        {
            dgvRectangle.RowCount = 7;
            dgvRectangle[0, 0].Value = "Width";
            dgvRectangle[0, 1].Value = "Height";
            dgvRectangle[0, 2].Value = "Slab Width";
            dgvRectangle[0, 3].Value = "Slab Thickness";
            dgvRectangle[0, 4].Value = "Haunch Width";
            dgvRectangle[0, 5].Value = "Haunch Depth";
            dgvRectangle[0, 6].Value = "Modular Ratio";
            dgvRectangle[1, 0].Value = 1;
            dgvRectangle[1, 1].Value = 1;
            dgvRectangle[1, 2].Value = 0;
            dgvRectangle[1, 3].Value = 0;
            dgvRectangle[1, 4].Value = 0;
            dgvRectangle[1, 5].Value = 0;
            dgvRectangle[1, 6].Value = 0;

            dgvCircle.RowCount = 1;
            dgvCircle[0, 0].Value = "Diameter";
            dgvCircle[1, 0].Value = 3;

            dgvIGirder.RowCount = 15;
            dgvIGirder[0, 0].Value = "H";
            dgvIGirder[0, 1].Value = "TFb";
            dgvIGirder[0, 2].Value = "TFto";
            dgvIGirder[0, 3].Value = "TFti";
            dgvIGirder[0, 4].Value = "tw";
            dgvIGirder[0, 5].Value = "BFb";
            dgvIGirder[0, 6].Value = "BFto";
            dgvIGirder[0, 7].Value = "BFti";
            dgvIGirder[0, 8].Value = "BFfh";
            dgvIGirder[0, 9].Value = "BFfv";
            dgvIGirder[0, 10].Value = "Slab Width";
            dgvIGirder[0, 11].Value = "Slab Thickness";
            dgvIGirder[0, 12].Value = "Haunch Width";
            dgvIGirder[0, 13].Value = "Haunch Depth";
            dgvIGirder[0, 14].Value = "Modular Ratio";
            dgvIGirder[1, 0].Value = 75;
            dgvIGirder[1, 1].Value = 42;
            dgvIGirder[1, 2].Value = 4;
            dgvIGirder[1, 3].Value = 8;
            dgvIGirder[1, 4].Value = 6;
            dgvIGirder[1, 5].Value = 24;
            dgvIGirder[1, 6].Value = 4;
            dgvIGirder[1, 7].Value = 6;
            dgvIGirder[1, 8].Value = 4;
            dgvIGirder[1, 9].Value = 6;
            dgvIGirder[1, 10].Value = 0;
            dgvIGirder[1, 11].Value = 0;
            dgvIGirder[1, 12].Value = 0;
            dgvIGirder[1, 13].Value = 0;
            dgvIGirder[1, 14].Value = 0;

            dgvBoxGirder.RowCount = 20;
            dgvBoxGirder[0, 0].Value = "W";
            dgvBoxGirder[0, 1].Value = "H";
            dgvBoxGirder[0, 2].Value = "TFt";
            dgvBoxGirder[0, 3].Value = "BFt";
            dgvBoxGirder[0, 4].Value = "# Int. Grdr";
            dgvBoxGirder[0, 5].Value = "twebi";
            dgvBoxGirder[0, 6].Value = "webtL";
            dgvBoxGirder[0, 7].Value = "webOffL";
            dgvBoxGirder[0, 8].Value = "webtR";
            dgvBoxGirder[0, 9].Value = "webOffR";
            dgvBoxGirder[0, 10].Value = "OHL";
            dgvBoxGirder[0, 11].Value = "OHLtout";
            dgvBoxGirder[0, 12].Value = "OHLtin";
            dgvBoxGirder[0, 13].Value = "OHR";
            dgvBoxGirder[0, 14].Value = "OHRtout";
            dgvBoxGirder[0, 15].Value = "OHRtin";
            dgvBoxGirder[0, 16].Value = "wFilletTop";
            dgvBoxGirder[0, 17].Value = "hFilletTop";
            dgvBoxGirder[0, 18].Value = "wFilletBot";
            dgvBoxGirder[0, 19].Value = "hFilletBot";
            dgvBoxGirder[1, 0].Value = 480;
            dgvBoxGirder[1, 1].Value = 54;
            dgvBoxGirder[1, 2].Value = 9;
            dgvBoxGirder[1, 3].Value = 8;
            dgvBoxGirder[1, 4].Value = 3;
            dgvBoxGirder[1, 5].Value = 12;
            dgvBoxGirder[1, 6].Value = 12;
            dgvBoxGirder[1, 7].Value = 36;
            dgvBoxGirder[1, 8].Value = 12;
            dgvBoxGirder[1, 9].Value = 36;
            dgvBoxGirder[1, 10].Value = 42;
            dgvBoxGirder[1, 11].Value = 6;
            dgvBoxGirder[1, 12].Value = 12;
            dgvBoxGirder[1, 13].Value = 42;
            dgvBoxGirder[1, 14].Value = 6;
            dgvBoxGirder[1, 15].Value = 12;
            dgvBoxGirder[1, 16].Value = 3;
            dgvBoxGirder[1, 17].Value = 4;
            dgvBoxGirder[1, 18].Value = 5;
            dgvBoxGirder[1, 19].Value = 6;

            dgvUGirder.RowCount = 14;
            dgvUGirder[0, 0].Value = "H";
            dgvUGirder[0, 1].Value = "Wb";
            dgvUGirder[0, 2].Value = "Wf";
            dgvUGirder[0, 3].Value = "OH";
            dgvUGirder[0, 4].Value = "Wo";
            dgvUGirder[0, 5].Value = "to";
            dgvUGirder[0, 6].Value = "ti";
            dgvUGirder[0, 7].Value = "ttop";
            dgvUGirder[0, 8].Value = "tbot";
            dgvUGirder[0, 9].Value = "Slab Width";
            dgvUGirder[0, 10].Value = "Slab Thickness";
            dgvUGirder[0, 11].Value = "Haunch Width";
            dgvUGirder[0, 12].Value = "Haunch Depth";
            dgvUGirder[0, 13].Value = "Modular Ratio";
            dgvUGirder[1, 0].Value = 65;
            dgvUGirder[1, 1].Value = 68;
            dgvUGirder[1, 2].Value = 14;
            dgvUGirder[1, 3].Value = 8;
            dgvUGirder[1, 4].Value = 6;
            dgvUGirder[1, 5].Value = 5;
            dgvUGirder[1, 6].Value = 8;
            dgvUGirder[1, 7].Value = 16;
            dgvUGirder[1, 8].Value = 13;
            dgvUGirder[1, 9].Value = 0;
            dgvUGirder[1, 10].Value = 0;
            dgvUGirder[1, 11].Value = 0;
            dgvUGirder[1, 12].Value = 0;
            dgvUGirder[1, 13].Value = 0;

            dgvPrecastBox.RowCount = 11;
            dgvPrecastBox[0, 0].Value = "H";
            dgvPrecastBox[0, 1].Value = "W";
            dgvPrecastBox[0, 2].Value = "ttop";
            dgvPrecastBox[0, 3].Value = "tbot";
            dgvPrecastBox[0, 4].Value = "tw";
            dgvPrecastBox[0, 5].Value = "fillet";
            dgvPrecastBox[0, 6].Value = "Slab Width";
            dgvPrecastBox[0, 7].Value = "Slab Thickness";
            dgvPrecastBox[0, 8].Value = "Haunch Width";
            dgvPrecastBox[0, 9].Value = "Haunch Depth";
            dgvPrecastBox[0, 10].Value = "Modular Ratio";
            dgvPrecastBox[1, 0].Value = 48;
            dgvPrecastBox[1, 1].Value = 60;
            dgvPrecastBox[1, 2].Value = 9;
            dgvPrecastBox[1, 3].Value = 6;
            dgvPrecastBox[1, 4].Value = 6;
            dgvPrecastBox[1, 5].Value = 5;
            dgvPrecastBox[1, 6].Value = 0;
            dgvPrecastBox[1, 7].Value = 0;
            dgvPrecastBox[1, 8].Value = 0;
            dgvPrecastBox[1, 9].Value = 0;
            dgvPrecastBox[1, 10].Value = 0;

            dgvRectangle.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvRectangle.AllowUserToResizeRows = false;
            dgvCircle.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvCircle.AllowUserToResizeRows = false;
            dgvIGirder.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvIGirder.AllowUserToResizeRows = false;
            dgvBoxGirder.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvBoxGirder.AllowUserToResizeRows = false;
            dgvUGirder.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvUGirder.AllowUserToResizeRows = false;
            dgvPrecastBox.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvPrecastBox.AllowUserToResizeRows = false;
            dgvVoidedSlab.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvVoidedSlab.AllowUserToResizeRows = false;
            dgvCABathTub.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvCABathTub.AllowUserToResizeRows = false;

            UpdateDialog();
            return;
        }

        private void UpdateDialog()
        {
            string getSecShape = STemp.SecShape();

            switch (getSecShape)
            {
                case "Rectangle":
                    cmbSelType.Text = "Rectangle";
                    Rectangle();
                    break;
                case "Circle":
                    cmbSelType.Text = "Circle";
                    Circle();
                    break;
                case "I_Girder":
                    cmbSelType.Text = "I-Girder";
                    IGirder();
                    break;
                case "Box_Girder":
                    cmbSelType.Text = "Box Girder";
                    BoxGirder();
                    break;
                case "U_Girder":
                    cmbSelType.Text = "U-Girder";
                    UGirder();
                    break;
                case "PC_Box":
                    cmbSelType.Text = "Precast Box";
                    PrecastBox();
                    break;
            }

            ShowSection();

            cmbUnitRectangle.Text = "in";
            cmbUnitCircle.Text = "in";
            cmbUnitIGirder.Text = "in";
            cmbUnitBoxGirder.Text = "in";
            cmbUnitUGirder.Text = "in";
            cmbUnitPrecastBox.Text = "in";
            return;
        }

        private void cmbSelType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbSelType.Text)
            {
                case "Rectangle":
                    cmbUnitRectangle.Visible = true;
                    cmbUnitCircle.Visible = false;
                    cmbUnitIGirder.Visible = false;
                    cmbUnitBoxGirder.Visible = false;
                    cmbUnitUGirder.Visible = false;
                    cmbUnitPrecastBox.Visible = false;

                    PicRectangle.Visible = true;
                    PicCircle.Visible = false;
                    PicIGirder.Visible = false;
                    PicBoxGirder.Visible = false;
                    PicUGirder.Visible = false;
                    PicPrecastBox.Visible = false;
                    PicVoidedSlab.Visible = false;
                    PicBathTub.Visible = false;

                    dgvRectangle.Visible = true;
                    dgvCircle.Visible = false;
                    dgvIGirder.Visible = false;
                    dgvBoxGirder.Visible = false;
                    dgvUGirder.Visible = false;
                    dgvPrecastBox.Visible = false;
                    dgvVoidedSlab.Visible = false;
                    dgvCABathTub.Visible = false;
                    Rectangle();
                    break;

                case "Circle":
                    cmbUnitRectangle.Visible = false;
                    cmbUnitCircle.Visible = true;
                    cmbUnitIGirder.Visible = false;
                    cmbUnitBoxGirder.Visible = false;
                    cmbUnitUGirder.Visible = false;
                    cmbUnitPrecastBox.Visible = false;

                    PicRectangle.Visible = false;
                    PicCircle.Visible = true;
                    PicIGirder.Visible = false;
                    PicBoxGirder.Visible = false;
                    PicUGirder.Visible = false;
                    PicPrecastBox.Visible = false;
                    PicVoidedSlab.Visible = false;
                    PicBathTub.Visible = false;

                    dgvRectangle.Visible = false;
                    dgvCircle.Visible = true;
                    dgvIGirder.Visible = false;
                    dgvBoxGirder.Visible = false;
                    dgvUGirder.Visible = false;
                    dgvPrecastBox.Visible = false;
                    dgvVoidedSlab.Visible = false;
                    dgvCABathTub.Visible = false;
                    Circle();
                    break;

                case "I-Girder":
                    cmbUnitRectangle.Visible = false;
                    cmbUnitCircle.Visible = false;
                    cmbUnitIGirder.Visible = true;
                    cmbUnitBoxGirder.Visible = false;
                    cmbUnitUGirder.Visible = false;
                    cmbUnitPrecastBox.Visible = false;

                    PicRectangle.Visible = false;
                    PicCircle.Visible = false;
                    PicIGirder.Visible = true;
                    PicBoxGirder.Visible = false;
                    PicUGirder.Visible = false;
                    PicPrecastBox.Visible = false;
                    PicVoidedSlab.Visible = false;
                    PicBathTub.Visible = false;

                    dgvRectangle.Visible = false;
                    dgvCircle.Visible = false;
                    dgvIGirder.Visible = true;
                    dgvBoxGirder.Visible = false;
                    dgvUGirder.Visible = false;
                    dgvPrecastBox.Visible = false;
                    dgvVoidedSlab.Visible = false;
                    dgvCABathTub.Visible = false;
                    IGirder();
                    break;

                case "Box Girder":
                    cmbUnitRectangle.Visible = false;
                    cmbUnitCircle.Visible = false;
                    cmbUnitIGirder.Visible = false;
                    cmbUnitBoxGirder.Visible = true;
                    cmbUnitUGirder.Visible = false;
                    cmbUnitPrecastBox.Visible = false;

                    PicRectangle.Visible = false;
                    PicCircle.Visible = false;
                    PicIGirder.Visible = false;
                    PicBoxGirder.Visible = true;
                    PicUGirder.Visible = false;
                    PicPrecastBox.Visible = false;
                    PicVoidedSlab.Visible = false;
                    PicBathTub.Visible = false;

                    dgvRectangle.Visible = false;
                    dgvCircle.Visible = false;
                    dgvIGirder.Visible = false;
                    dgvBoxGirder.Visible = true;
                    dgvUGirder.Visible = false;
                    dgvPrecastBox.Visible = false;
                    dgvVoidedSlab.Visible = false;
                    dgvCABathTub.Visible = false;
                    BoxGirder();
                    break;

                case "U-Girder":
                    cmbUnitRectangle.Visible = false;
                    cmbUnitCircle.Visible = false;
                    cmbUnitIGirder.Visible = false;
                    cmbUnitBoxGirder.Visible = false;
                    cmbUnitUGirder.Visible = true;
                    cmbUnitPrecastBox.Visible = false;

                    PicRectangle.Visible = false;
                    PicCircle.Visible = false;
                    PicIGirder.Visible = false;
                    PicBoxGirder.Visible = false;
                    PicUGirder.Visible = true;
                    PicPrecastBox.Visible = false;
                    PicVoidedSlab.Visible = false;
                    PicBathTub.Visible = false;

                    dgvRectangle.Visible = false;
                    dgvCircle.Visible = false;
                    dgvIGirder.Visible = false;
                    dgvBoxGirder.Visible = false;
                    dgvUGirder.Visible = true;
                    dgvPrecastBox.Visible = false;
                    dgvVoidedSlab.Visible = false;
                    dgvCABathTub.Visible = false;
                    UGirder();
                    break;

                case "Precast Box":
                    cmbUnitRectangle.Visible = false;
                    cmbUnitCircle.Visible = false;
                    cmbUnitIGirder.Visible = false;
                    cmbUnitBoxGirder.Visible = false;
                    cmbUnitUGirder.Visible = false;
                    cmbUnitPrecastBox.Visible = true;

                    PicRectangle.Visible = false;
                    PicCircle.Visible = false;
                    PicIGirder.Visible = false;
                    PicBoxGirder.Visible = false;
                    PicUGirder.Visible = false;
                    PicPrecastBox.Visible = true;
                    PicVoidedSlab.Visible = false;
                    PicBathTub.Visible = false;

                    dgvRectangle.Visible = false;
                    dgvCircle.Visible = false;
                    dgvIGirder.Visible = false;
                    dgvBoxGirder.Visible = false;
                    dgvUGirder.Visible = false;
                    dgvPrecastBox.Visible = true;
                    dgvVoidedSlab.Visible = false;
                    dgvCABathTub.Visible = false;
                    PrecastBox();
                    break;

                //case "Voided Slab":
                //    dgvVoidedSlab.RowCount = 12;

                //    dgvVoidedSlab[0, 0].Value = "L";
                //    dgvVoidedSlab[0, 1].Value = "H";
                //    dgvVoidedSlab[0, 2].Value = "L1";
                //    dgvVoidedSlab[0, 3].Value = "L2";
                //    dgvVoidedSlab[0, 4].Value = "Nvoid";
                //    dgvVoidedSlab[0, 5].Value = "D1";
                //    dgvVoidedSlab[0, 6].Value = "D2";
                //    dgvVoidedSlab[0, 7].Value = "kh1";
                //    dgvVoidedSlab[0, 8].Value = "kv1";
                //    dgvVoidedSlab[0, 9].Value = "kh2";
                //    dgvVoidedSlab[0, 10].Value = "kv2";
                //    dgvVoidedSlab[0, 11].Value = "kslope";

                //    dgvRectangle.Visible = false;
                //    dgvCircle.Visible = false;
                //    dgvIGirder.Visible = false;
                //    dgvBoxGirder.Visible = false;
                //    dgvUGirder.Visible = false;
                //    dgvPrecastBox.Visible = false;
                //    dgvVoidedSlab.Visible = true;
                //    gdvCABathTub.Visible = false;
                //    break;

                //case "CA BathTub":
                //    gdvCABathTub.RowCount = 16;

                //    gdvCABathTub[0, 0].Value = "W-Bot";
                //    gdvCABathTub[0, 1].Value = "D";
                //    gdvCABathTub[0, 2].Value = "Web thickness";
                //    gdvCABathTub[0, 3].Value = "Web Slope";
                //    gdvCABathTub[0, 4].Value = "Bot Flange";
                //    gdvCABathTub[0, 5].Value = "Fillet";
                //    gdvCABathTub[0, 6].Value = "TFb";
                //    gdvCABathTub[0, 7].Value = "Tft1";
                //    gdvCABathTub[0, 8].Value = "Tft2";
                //    gdvCABathTub[0, 9].Value = "kh1";
                //    gdvCABathTub[0, 10].Value = "kv1";
                //    gdvCABathTub[0, 11].Value = "Slab Width";
                //    gdvCABathTub[0, 12].Value = "Slab Thickness";
                //    gdvCABathTub[0, 13].Value = "Haunch Width";
                //    gdvCABathTub[0, 14].Value = "Haunch Depth";
                //    gdvCABathTub[0, 15].Value = "Modular Ratio";

                //    dgvRectangle.Visible = false;
                //    dgvCircle.Visible = false;
                //    dgvIGirder.Visible = false;
                //    dgvBoxGirder.Visible = false;
                //    dgvUGirder.Visible = false;
                //    dgvPrecastBox.Visible = false;
                //    dgvVoidedSlab.Visible = false;
                //    gdvCABathTub.Visible = true;
                //    break;

                //case "Parts":

                //break;
            }
            cmbUnitRectangle.Text = "in";
            cmbUnitCircle.Text = "in";
            cmbUnitIGirder.Text = "in";
            cmbUnitBoxGirder.Text = "in";
            cmbUnitUGirder.Text = "in";
            cmbUnitPrecastBox.Text = "in";
        }

        private void dgvRectangle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Rectangle();
        }

        private void dgvCircle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Circle();
        }

        private void dgvIGirder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            IGirder();
        }

        private void dgvBoxGirder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            BoxGirder();
        }

        private void dgvUGirder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            UGirder();
        }

        private void dgvPrecastBox_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            PrecastBox();
        }

        public void Rectangle()
        {
            m_View = new Kanvas2D();
            m_View.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_View.AddPen(new Pen(Color.Green));
            try
            {
            double[] dims = new double[2];
            dims[0] = Convert.ToDouble(dgvRectangle[1, 0].Value);
            dims[1] = Convert.ToDouble(dgvRectangle[1, 1].Value);
            double[,] XY;
            XY = STemp.SecPointsByShape("Rectangle", dims, false, null);
            m_View.AddPolygon(XY, 1, 0);
            m_View.ZoomFit();
            pbModel.Image = m_View.GetPicture();
            }
            catch
            {
                MessageBox.Show("Input value was not in a correct format.", "Error");
            } 
        }

        private void Circle()
        {
            m_View = new Kanvas2D();
            m_View.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_View.AddPen(new Pen(Color.Green));
            try
            {
            double[] dims = new double[1];
            dims[0] = Convert.ToDouble(dgvCircle[1, 0].Value);
            double[,] XY;
            XY = STemp.SecPointsByShape("Circle", dims, false, null);
            m_View.AddPolygon(XY, 1, 0);
            m_View.ZoomFit();
            pbModel.Image = m_View.GetPicture();
            }
            catch
            {
                MessageBox.Show("Input value was not in a correct format.", "Error");
            }
        }

        private void IGirder()
        {
            m_View = new Kanvas2D();
            m_View.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_View.AddPen(new Pen(Color.Green));
            try
            {
            double[] dims = new double[10];
            dims[0] = Convert.ToDouble(dgvIGirder[1, 0].Value);
            dims[1] = Convert.ToDouble(dgvIGirder[1, 1].Value);
            dims[2] = Convert.ToDouble(dgvIGirder[1, 2].Value);
            dims[3] = Convert.ToDouble(dgvIGirder[1, 3].Value);
            dims[4] = Convert.ToDouble(dgvIGirder[1, 4].Value);
            dims[5] = Convert.ToDouble(dgvIGirder[1, 5].Value);
            dims[6] = Convert.ToDouble(dgvIGirder[1, 6].Value);
            dims[7] = Convert.ToDouble(dgvIGirder[1, 7].Value);
            dims[8] = Convert.ToDouble(dgvIGirder[1, 8].Value);
            dims[9] = Convert.ToDouble(dgvIGirder[1, 9].Value);

            double[,] XY;
            XY = STemp.SecPointsByShape("I_Girder", dims, false, null);
            m_View.AddPolygon(XY, 1, 0);

            m_View.ZoomFit();
            pbModel.Image = m_View.GetPicture();
            }
            catch
            {
                MessageBox.Show("Input value was not in a correct format.", "Error");
            }
        }

        private void BoxGirder()
        {
            m_View = new Kanvas2D();
            m_View.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_View.AddPen(new Pen(Color.Green));
            try
            {
            double[] dims = new double[20];
            dims[0] = Convert.ToDouble(dgvBoxGirder[1, 0].Value);
            dims[1] = Convert.ToDouble(dgvBoxGirder[1, 1].Value);
            dims[2] = Convert.ToDouble(dgvBoxGirder[1, 2].Value);
            dims[3] = Convert.ToDouble(dgvBoxGirder[1, 3].Value);
            dims[4] = Convert.ToDouble(dgvBoxGirder[1, 4].Value);
            dims[5] = Convert.ToDouble(dgvBoxGirder[1, 5].Value);
            dims[6] = Convert.ToDouble(dgvBoxGirder[1, 6].Value);
            dims[7] = Convert.ToDouble(dgvBoxGirder[1, 7].Value);
            dims[8] = Convert.ToDouble(dgvBoxGirder[1, 8].Value);
            dims[9] = Convert.ToDouble(dgvBoxGirder[1, 9].Value);
            dims[10] = Convert.ToDouble(dgvBoxGirder[1, 10].Value);
            dims[11] = Convert.ToDouble(dgvBoxGirder[1, 11].Value);
            dims[12] = Convert.ToDouble(dgvBoxGirder[1, 12].Value);
            dims[13] = Convert.ToDouble(dgvBoxGirder[1, 13].Value);
            dims[14] = Convert.ToDouble(dgvBoxGirder[1, 14].Value);
            dims[15] = Convert.ToDouble(dgvBoxGirder[1, 15].Value);
            dims[16] = Convert.ToDouble(dgvBoxGirder[1, 16].Value);
            dims[17] = Convert.ToDouble(dgvBoxGirder[1, 17].Value);
            dims[18] = Convert.ToDouble(dgvBoxGirder[1, 18].Value);
            dims[19] = Convert.ToDouble(dgvBoxGirder[1, 19].Value);

            double[,] XY;
            XY = STemp.SecPointsByShape("Box_Girder", dims, false, null);
            m_View.AddPolygon(XY, 1, 0);

            m_View.ZoomFit();
            pbModel.Image = m_View.GetPicture();
            }
            catch
            {
                MessageBox.Show("Input value was not in a correct format.", "Error");
            }
        }

        private void UGirder()
        {
            m_View = new Kanvas2D();
            m_View.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_View.AddPen(new Pen(Color.Green));
            try
            {
            double[] dims = new double[9];
            dims[0] = Convert.ToDouble(dgvUGirder[1, 0].Value);
            dims[1] = Convert.ToDouble(dgvUGirder[1, 1].Value);
            dims[2] = Convert.ToDouble(dgvUGirder[1, 2].Value);
            dims[3] = Convert.ToDouble(dgvUGirder[1, 3].Value);
            dims[4] = Convert.ToDouble(dgvUGirder[1, 4].Value);
            dims[5] = Convert.ToDouble(dgvUGirder[1, 5].Value);
            dims[6] = Convert.ToDouble(dgvUGirder[1, 6].Value);
            dims[7] = Convert.ToDouble(dgvUGirder[1, 7].Value);
            dims[8] = Convert.ToDouble(dgvUGirder[1, 8].Value);

            double[,] XY;
            XY = STemp.SecPointsByShape("U_Girder", dims, false, null);
            XY[0, 0] = XY[1, 0];
            XY[XY.GetLength(0) - 1, 0] = XY[XY.GetLength(0) - 2, 0];
            m_View.AddPolygon(XY, 1, 0);

            m_View.ZoomFit();
            pbModel.Image = m_View.GetPicture();
            }
            catch
            {
                MessageBox.Show("Input value was not in a correct format.", "Error");
            }
        }

        private void PrecastBox()
        {
            m_View = new Kanvas2D();
            m_View.SetWinSize(pbModel.Size.Width, pbModel.Size.Height);
            m_View.AddPen(new Pen(Color.Green));
            try
            {
            double[] dims = new double[6];
            dims[0] = Convert.ToDouble(dgvPrecastBox[1, 0].Value);
            dims[1] = Convert.ToDouble(dgvPrecastBox[1, 1].Value);
            dims[2] = Convert.ToDouble(dgvPrecastBox[1, 2].Value);
            dims[3] = Convert.ToDouble(dgvPrecastBox[1, 3].Value);
            dims[4] = Convert.ToDouble(dgvPrecastBox[1, 4].Value);
            dims[5] = Convert.ToDouble(dgvPrecastBox[1, 5].Value);

            double[,] XY;
            XY = STemp.SecPointsByShape("PC_Box", dims, false, null);
            m_View.AddPolygon(XY, 1, 0);

            m_View.ZoomFit();
            pbModel.Image = m_View.GetPicture();
            }
            catch
            {
                MessageBox.Show("Input value was not in a correct format.", "Error");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            m_OK = false;
            this.Close();
            return;
        }

        private void cmbUnitRectangle_SelectedValueChanged(object sender, EventArgs e)
        {
            if (strUnit != cmbUnitRectangle.Text)
            {
                switch (strUnit)
                {
                    case "in":
                        for (int i = 0; i <= 1; i++)
                        {
                            dgvRectangle[1, i].Value = (Convert.ToDouble(dgvRectangle[1, i].Value)) / 12;
                        }
                        break;
                    case "ft":
                        for (int i = 0; i <= 1; i++)
                        {
                            dgvRectangle[1, i].Value = (Convert.ToDouble(dgvRectangle[1, i].Value)) * 12;
                        }
                        break;
                }
            }
        }

        private void cmbUnitRectangle_SelectedIndexChanged(object sender, EventArgs e)
        {
            strUnit = cmbUnitRectangle.Text;
        }

        private void cmbUnitCircle_SelectedValueChanged_1(object sender, EventArgs e)
        {
            if (strUnit != cmbUnitCircle.Text)
            {
                switch (strUnit)
                {
                    case "in":
                        dgvCircle[1, 0].Value = (Convert.ToDouble(dgvCircle[1, 0].Value)) / 12;
                        break;
                    case "ft":
                        dgvCircle[1, 0].Value = (Convert.ToDouble(dgvCircle[1, 0].Value)) * 12;
                        break;
                }
            }
        }

        private void cmbUnitCircle_SelectedIndexChanged(object sender, EventArgs e)
        {
            strUnit = cmbUnitCircle.Text;
        }

        private void cmbUnitIGirder_SelectedValueChanged(object sender, EventArgs e)
        {
            if (strUnit != cmbUnitIGirder.Text)
            {
                switch (strUnit)
                {
                    case "in":
                        for (int i = 0; i <= 14; i++)
                        {
                            dgvIGirder[1, i].Value = (Convert.ToDouble(dgvIGirder[1, i].Value)) / 12;
                        }
                        break;
                    case "ft":
                        for (int i = 0; i <= 14; i++)
                        {
                            dgvIGirder[1, i].Value = (Convert.ToDouble(dgvIGirder[1, i].Value)) * 12;
                        }
                        break;
                }
            }
        }

        private void cmbUnitIGirder_SelectedIndexChanged(object sender, EventArgs e)
        {
            strUnit = cmbUnitIGirder.Text;
        }

        private void cmbUnitBoxGirder_SelectedValueChanged(object sender, EventArgs e)
        {
            if (strUnit != cmbUnitBoxGirder.Text)
            {
                switch (strUnit)
                {
                    case "in":
                        for (int i = 0; i <= 19; i++)
                        {
                            if (i != 4) dgvBoxGirder[1, i].Value = (Convert.ToDouble(dgvBoxGirder[1, i].Value)) / 12;
                        }
                        break;
                    case "ft":
                        for (int i = 0; i <= 19; i++)
                        {
                            if (i != 4) dgvBoxGirder[1, i].Value = (Convert.ToDouble(dgvBoxGirder[1, i].Value)) * 12;
                        }
                        break;
                }
            }
        }

        private void cmbUnitBoxGirder_SelectedIndexChanged(object sender, EventArgs e)
        {
            strUnit = cmbUnitBoxGirder.Text;
        }

        private void cmbUnitUGirder_SelectedValueChanged(object sender, EventArgs e)
        {
            if (strUnit != cmbUnitUGirder.Text)
            {
                switch (strUnit)
                {
                    case "in":
                        for (int i = 0; i <= 13; i++)
                        {
                            dgvUGirder[1, i].Value = (Convert.ToDouble(dgvUGirder[1, i].Value)) / 12;
                        }
                        break;
                    case "ft":
                        for (int i = 0; i <= 13; i++)
                        {
                            dgvUGirder[1, i].Value = (Convert.ToDouble(dgvUGirder[1, i].Value)) * 12;
                        }
                        break;
                }
            }
        }

        private void cmbUnitUGirder_SelectedIndexChanged(object sender, EventArgs e)
        {
            strUnit = cmbUnitUGirder.Text;
        }

        private void cmbUnitPrecastBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (strUnit != cmbUnitPrecastBox.Text)
            {
                switch (strUnit)
                {
                    case "in":
                        for (int i = 0; i <= 10; i++)
                        {
                            dgvPrecastBox[1, i].Value = (Convert.ToDouble(dgvPrecastBox[1, i].Value)) / 12;
                        }
                        break;
                    case "ft":
                        for (int i = 0; i <= 10; i++)
                        {
                            dgvPrecastBox[1, i].Value = (Convert.ToDouble(dgvPrecastBox[1, i].Value)) * 12;
                        }
                        break;
                }
            }
        }

        private void cmbUnitPrecastBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            strUnit = cmbUnitPrecastBox.Text;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            m_OK = true;
            SaveSection();
            this.Close();
            return;
        }

        private void btnProperties_Click(object sender, EventArgs e)
        {
            frmProperties fProperties = new frmProperties();
            fProperties.getType = cmbSelType.Text;

            switch (cmbSelType.Text)
            {
                case "Rectangle":
                    double[] dimsRec = new double[2];
                    dimsRec[0] = Convert.ToDouble(dgvRectangle[1, 0].Value);
                    dimsRec[1] = Convert.ToDouble(dgvRectangle[1, 1].Value);
                    STemp.SetSectionShape("Rectangle", dimsRec, false, null, true);
                    fProperties.Prop0 = STemp.area;
                    fProperties.Prop1 = STemp.area22;
                    fProperties.Prop2 = STemp.area33;
                    fProperties.Prop3 = STemp.J;
                    fProperties.Prop4 = STemp.I22;
                    fProperties.Prop5 = STemp.I33;
                    fProperties.Prop6 = STemp.DepthZ;
                    fProperties.Prop7 = STemp.CGZ;
                    break;
                case "Circle":
                    double[] dimsCir = new double[1];
                    dimsCir[0] = Convert.ToDouble(dgvCircle[1, 0].Value);
                    STemp.SetSectionShape("Circle", dimsCir, false, null, true);
                    fProperties.Prop0 = STemp.area;
                    fProperties.Prop1 = STemp.area22;
                    fProperties.Prop2 = STemp.area33;
                    fProperties.Prop3 = STemp.J;
                    fProperties.Prop4 = STemp.I22;
                    fProperties.Prop5 = STemp.I33;
                    fProperties.Prop6 = STemp.DepthZ;
                    fProperties.Prop7 = STemp.CGZ;
                    break;
                case "I-Girder":
                    double[] dimsIG = new double[10];
                    dimsIG[0] = Convert.ToDouble(dgvIGirder[1, 0].Value);
                    dimsIG[1] = Convert.ToDouble(dgvIGirder[1, 1].Value);
                    dimsIG[2] = Convert.ToDouble(dgvIGirder[1, 2].Value);
                    dimsIG[3] = Convert.ToDouble(dgvIGirder[1, 3].Value);
                    dimsIG[4] = Convert.ToDouble(dgvIGirder[1, 4].Value);
                    dimsIG[5] = Convert.ToDouble(dgvIGirder[1, 5].Value);
                    dimsIG[6] = Convert.ToDouble(dgvIGirder[1, 6].Value);
                    dimsIG[7] = Convert.ToDouble(dgvIGirder[1, 7].Value);
                    dimsIG[8] = Convert.ToDouble(dgvIGirder[1, 8].Value);
                    dimsIG[9] = Convert.ToDouble(dgvIGirder[1, 9].Value);
                    STemp.SetSectionShape("I_Girder", dimsIG, false, null, true);
                    fProperties.Prop0 = STemp.area;
                    fProperties.Prop1 = STemp.area22;
                    fProperties.Prop2 = STemp.area33;
                    fProperties.Prop3 = STemp.J;
                    fProperties.Prop4 = STemp.I22;
                    fProperties.Prop5 = STemp.I33;
                    fProperties.Prop6 = STemp.DepthZ;
                    fProperties.Prop7 = STemp.CGZ;
                    break;
                case "Box Girder":
                    double[] dimsBG = new double[20];
                    dimsBG[0] = Convert.ToDouble(dgvBoxGirder[1, 0].Value);
                    dimsBG[1] = Convert.ToDouble(dgvBoxGirder[1, 1].Value);
                    dimsBG[2] = Convert.ToDouble(dgvBoxGirder[1, 2].Value);
                    dimsBG[3] = Convert.ToDouble(dgvBoxGirder[1, 3].Value);
                    dimsBG[4] = Convert.ToDouble(dgvBoxGirder[1, 4].Value);
                    dimsBG[5] = Convert.ToDouble(dgvBoxGirder[1, 5].Value);
                    dimsBG[6] = Convert.ToDouble(dgvBoxGirder[1, 6].Value);
                    dimsBG[7] = Convert.ToDouble(dgvBoxGirder[1, 7].Value);
                    dimsBG[8] = Convert.ToDouble(dgvBoxGirder[1, 8].Value);
                    dimsBG[9] = Convert.ToDouble(dgvBoxGirder[1, 9].Value);
                    dimsBG[10] = Convert.ToDouble(dgvBoxGirder[1, 10].Value);
                    dimsBG[11] = Convert.ToDouble(dgvBoxGirder[1, 11].Value);
                    dimsBG[12] = Convert.ToDouble(dgvBoxGirder[1, 12].Value);
                    dimsBG[13] = Convert.ToDouble(dgvBoxGirder[1, 13].Value);
                    dimsBG[14] = Convert.ToDouble(dgvBoxGirder[1, 14].Value);
                    dimsBG[15] = Convert.ToDouble(dgvBoxGirder[1, 15].Value);
                    dimsBG[16] = Convert.ToDouble(dgvBoxGirder[1, 16].Value);
                    dimsBG[17] = Convert.ToDouble(dgvBoxGirder[1, 17].Value);
                    dimsBG[18] = Convert.ToDouble(dgvBoxGirder[1, 18].Value);
                    dimsBG[19] = Convert.ToDouble(dgvBoxGirder[1, 19].Value);
                    STemp.SetSectionShape("Box_Girder", dimsBG, false, null, true);
                    fProperties.Prop0 = STemp.area;
                    fProperties.Prop1 = STemp.area22;
                    fProperties.Prop2 = STemp.area33;
                    fProperties.Prop3 = STemp.J;
                    fProperties.Prop4 = STemp.I22;
                    fProperties.Prop5 = STemp.I33;
                    fProperties.Prop6 = STemp.DepthZ;
                    fProperties.Prop7 = STemp.CGZ;
                    break;
                case "U-Girder":
                    double[] dimsUG = new double[9];
                    dimsUG[0] = Convert.ToDouble(dgvUGirder[1, 0].Value);
                    dimsUG[1] = Convert.ToDouble(dgvUGirder[1, 1].Value);
                    dimsUG[2] = Convert.ToDouble(dgvUGirder[1, 2].Value);
                    dimsUG[3] = Convert.ToDouble(dgvUGirder[1, 3].Value);
                    dimsUG[4] = Convert.ToDouble(dgvUGirder[1, 4].Value);
                    dimsUG[5] = Convert.ToDouble(dgvUGirder[1, 5].Value);
                    dimsUG[6] = Convert.ToDouble(dgvUGirder[1, 6].Value);
                    dimsUG[7] = Convert.ToDouble(dgvUGirder[1, 7].Value);
                    dimsUG[8] = Convert.ToDouble(dgvUGirder[1, 8].Value);
                    STemp.SetSectionShape("U_Girder", dimsUG, false, null, true);
                    fProperties.Prop0 = STemp.area;
                    fProperties.Prop1 = STemp.area22;
                    fProperties.Prop2 = STemp.area33;
                    fProperties.Prop3 = STemp.J;
                    fProperties.Prop4 = STemp.I22;
                    fProperties.Prop5 = STemp.I33;
                    fProperties.Prop6 = STemp.DepthZ;
                    fProperties.Prop7 = STemp.CGZ;
                    break;
                case "Precast Box":
                    double[] dimsPB = new double[6];
                    dimsPB [0] = Convert.ToDouble(dgvPrecastBox[1, 0].Value);
                    dimsPB [1] = Convert.ToDouble(dgvPrecastBox[1, 1].Value);
                    dimsPB [2] = Convert.ToDouble(dgvPrecastBox[1, 2].Value);
                    dimsPB [3] = Convert.ToDouble(dgvPrecastBox[1, 3].Value);
                    dimsPB [4] = Convert.ToDouble(dgvPrecastBox[1, 4].Value);
                    dimsPB [5] = Convert.ToDouble(dgvPrecastBox[1, 5].Value);
                    STemp.SetSectionShape("PC_Box", dimsPB, false, null, true);
                    fProperties.Prop0 = STemp.area;
                    fProperties.Prop1 = STemp.area22;
                    fProperties.Prop2 = STemp.area33;
                    fProperties.Prop3 = STemp.J;
                    fProperties.Prop4 = STemp.I22;
                    fProperties.Prop5 = STemp.I33;
                    fProperties.Prop6 = STemp.DepthZ;
                    fProperties.Prop7 = STemp.CGZ;
                    break;
            }
            fProperties.ShowDialog();
        }

        private void btnLibSection_Click(object sender, EventArgs e)
        {
            frmLibrarySection fLibrarySection = new frmLibrarySection();
            fLibrarySection.ShowDialog();

            if (fLibrarySection.m_OK)
            {
                cmbUnitRectangle.Text = "in";
                cmbUnitCircle.Text = "in";
                cmbUnitIGirder.Text = "in";
                cmbUnitBoxGirder.Text = "in";
                cmbUnitUGirder.Text = "in";
                cmbUnitPrecastBox.Text = "in";

                STemp = fLibrarySection.STemp;
                UpdateDialog();
                
                switch (cmbSelType.Text)
                {
                    case "Rectangle":
                        Rectangle();
                        break;
                    case "Circle":
                        Circle();
                        break;
                    case "I-Girder":
                        IGirder();
                        break;
                    case "Box Girder":
                        BoxGirder();
                        break;
                    case "U-Girder":
                        UGirder();
                        break;
                    case "Precast Box":
                        PrecastBox();
                        break;
                }
                return;
            }
        }

    }
}
